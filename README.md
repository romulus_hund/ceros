## Table of contents

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
- [Technical details](#technical-details)
  - [Tech stack](#tech-stack)
  - [Main flow](#main-flow)
  - [Authentication](#authentication)
  - [Store structure](#store-structure)
  - [S3 Structure](#s3-structure)
  - [PHP Bridge](#php-bridge)
  - [API structure](#api-structure)
  - [Jenkins flow](#jenkins-flow)
- [CI Pipeline](#ci-pipeline)
- [Testing](#testing-components)
    - [Testing React components](#testing-components)
    - [Testing components with MobX dependencies](#testing-components-mobx)
        - [Recommended approach](#recommended-approach)
        - [Possible approaches](#possible-approaches)


## Introduction
### Ceros Publisher

UI for uploading Ceros micro-sites. 

## Technical Details

Tech stack
- [NodeJS](https://nodejs.org/en/).
- [PHP](https://www.php.net/).
- [Amazon S3](https://aws.amazon.com/sdk-for-php/).
- [NextJS](https://nextjs.org/) 
- [React](https://reactjs.org/) - used with NextJS to render components on the server and client.
- [MobX](https://mobx.js.org/) - state manager.
- [Styled Components](https://www.styled-components.com/) - for CSS styling.
- [Jest](https://jestjs.io/) - for unit tests.
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) - for testing react components.


### Run the app

Note: Make sure you have VPN installed and connected to the right environment (dev or prod).

**Development mode:**

1. `npm install`
2. `npm run dev`
3. Go to `http://localhost:3000/`

#Main flow

##Fetch existing micro-sites data
1. Fetch the list of micro-sites.
2. Fetch the list of of versions for each micro-site.
3. Fetch current deployed versions in staging and production environment for each micro-site.

##Upload new micro-site version
1. Upload new version of a micro-site.
2. Change micro-site version status.
2. Get version deployment status (success, failed, pending).

##Authentication
[Auth0](https://auth0.com/) authentication service.

##Store structure
We use MobX as a state manager. Store initialization is inside lib/app/pages/_app.js file.

Note: all store methods starts with _ (underscore) are internal class methods only. 

## S3 Structure
We use Amazon s3 service as an app data store.

Go to [S3 service](https://s3.console.aws.amazon.com/s3/home?region=us-east-1) and search for "ceros-publisher"
bucket (ask devops for credentials).

Inside this bucket there is a list of uploaded micro-sites.

Each folder gets it's name programmatically from configuration json file, which is sent to Jenkins
along with tar.gz file.

Inside each micro-site folder there is a list of micro-site versions. Each folder programmatically
named by this convention: ${micro-site name}-${micro-site version}. Each folder contains an uploaded
tar.gz file and config.json file with micro-site version details.

Each micro-site folder also contains "production" and "staging" folders, with corresponding
micro-site (config.json) inside.  

Communication with S3 is via PHP bridge, using [S3 PHP SDK](https://aws.amazon.com/sdk-for-php/).


## PHP Bridge
The whole API written in Node.js, however, due to security reasons, there are some PHP services, responsible for S3 communication,
fetching the bucket data in particular. The constructor inside aws-credentials.php responsible
for taking the S3 credentials from your local machine (.aws folder) in development environment or
roles from server machine in production. PHP code is an executable we run from the node app, running on the same server,
and outputs the following data, using get_list_of_s3_objects and getConfigs from aws-credentials.php:
1. List of micro-sites.
2. List of micro-sites versions per each micro-site.
3. Version status (staging or production).
4. Micro-site production version.
5. Micro-site staging version. 

##API structure
REST API with following endpoints:
- '/api/sites/current-deployed/' (PHP bridge) - get deployed production
and staging versions for a specified micro-site name.
- '/api/sites/' (PHP bridge) - get list of micro-sites.
- '/api/sites/versions' (PHP bridge) - get list of versions for specified micro-site.
- '/api/upload' - upload new version/change existing version status.
- '/api/version-status' - get uploaded/changed version deployment status
(success, failed, pending).

##Jenkins flow
We use Jenkins automation server to build, test, and deploy the Publisher App.
There are 2 scenarios when we trigger Jenkins services:

1) Upload new micro-site version (.tar.gz) - "/api/upload" endpoint. The uploaded
 .tar.gz triggers Jenkins jobs that set .tar.gz and it's config.json file in corresponding folder in S3 Bucket.
    
    
2) Set/change micro-site version status (send it to staging or production environment) - 
same as step 1, but Jenkins gets only config.json (no .tar.gz needed, since it's already exists in S# bucket).

###The upload flow goes like this: 
    
   a. Upload .tar.gz file via upload module.
   
   b. Send uploaded .tar.gz to the server.
   
   c. Extract .tar.gz to get index.html which contains micro-site metadata.
   
   d. Make config.js with site metadata.
   
   e. Send .tar.gz and config.json to Jenkins.


# CI Pipeline

-   The CI Pipeline works by hooking into the Version Control flow,
to trigger build, test and deploy scripts based on environment. 
-   We use [Jenkins](https://en.wikipedia.org/wiki/Jenkins_(software)) to orchestrate the build process. 

For each stage in the CI Pipeline, there are different approvals and checks that are requirements before code can be deployed to the next stage of development / deployment, detailed below:

-   Branch must be up to date with base (master branch).
-   ESLint of branch must pass.
-   Tests must pass and has appropriate additional cover.



# Testing

## Libraries

Before starting, would suggest some essential initial reading on the following topics:

  - [Jest](https://jestjs.io/) A test runner with snapshot testing and code coverage tools extending Jasmine
  - [React Testing Library](https://testing-library.com/) - for testing react components.


Test files are called 'specs' and live in a child folder of their subject called `__tests__` - Jest will look in these folders accross the app for test files and execute them synchronously when running the `npm test` command.

The tests themselves are characterised by a `describe` wrapper function around all tests for a file or class and numerous `it` functions that map to blocks of assertions. An assertion is usually characterised by an `expect` statement, where I make a statement about the state of my app or return value of a function, and test it for truthyness by comparison.

What to test should be agreed collectively however, a reasonable guide is;
  - Critical app paths that have the most traffic should be tested the most thoroughly i.e. code that effects the Article, Location and Home pages.
  - Tests that have the most value are where the code has the most logic, or where a break in the code will actually damage a page's ability to render e.g. API Services, Stores, Pages & complex Components.
  - For basic "dumb", stateless components with little or no conditional logic or simply styles, a basic DOM snapshot test is adequate, using Jest's in-built snapshot tools, [see below](#snapshots). 
  - If you are fixing a JS bug, if possible always add test(s) to avoid regression.
  - Always, try to leave the code with more coverage than you started with. If you are doing any PR, include some tests for the code you think is most complex, logic heavy or mission critical.

## Testing React components

For React tests, typically you will `render` component with RTL. 

```js
import { Provider } from "mobx-react";
import { render, fireEvent, wait } from '@testing-library/react';

const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
```
### An example how to test react component:

```js
import React from 'react'
import { render, fireEvent, waitForElement } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import axiosMock from 'axios'
import Fetch from '../fetch'

jest.mock('axios')

test('loads and displays greeting', async () => {
  const url = '/greeting'
  const { getByText, getByRole } = render(<Fetch url={url} />)

  axiosMock.get.mockResolvedValueOnce({
    data: { greeting: 'hello there' },
  })

  fireEvent.click(getByText('Load Greeting'))

  const greetingTextNode = await waitForElement(() => getByRole('heading'))

  expect(axiosMock.get).toHaveBeenCalledTimes(1)
  expect(axiosMock.get).toHaveBeenCalledWith(url)
  expect(getByRole('heading')).toHaveTextContent('hello there')
  expect(getByRole('button')).toHaveAttribute('disabled')
})
```
 

# Testing components with MobX dependencies

As you might have seen while writing unit tests, components which consume MobX stores with the `@inject` decorator will generally throw an exception when rendered with Enzyme, whether using `shallow`, `render`, or `mount`:

`Error: MobX injector: Store STORE_NAME is not available! Make sure it is provided by some Provider`

This happens because `@inject` is expecting to find the required stores in React's [(Legacy) Context](https://reactjs.org/docs/legacy-context.html). In the real application, we would find a `<Provider />` somewhere above our component in the tree responsible for making the required stores available. Since our components are tested in isolation - and therefore without any parent elements like `<Provider />` - we need to put the stores into Context ourselves.

We should keep in mind that if we are able to separate the "presentational" aspect of our component (the "dumb" part which just receives props and returns elements) from it's integration with MobX, we can write most of our tests without needing to worry about where the store is coming from by testing the "presentational" part directly ([see an example of this in the codebase](https://github.com/culture-trip/universal-app/blob/develop/components/hotel-date-picker/bookable-button/affiliate-row/hotel-price.js)). Once we have that separation, the techniques outlined below are only necessary for testing the integration. 

## Recommended approach

### ✅ [Using a real `<Provider />` from `mobx-react`](#3-using-a-real-provider--from-mobx-react)

## Possible approaches

Without changing the structure of your code to separate the MobX connection from the presentational aspect of your component, there are a number approaches that will get your component rendering in a unit test:

### 1. Pass the required stores as props

While MobX's `@inject` decorator is designed to look for your stores in Context, it will also accept stores passed in directly as props to your component. 

#### Pros:
* By far the simplest
* Familiar for those with experience with dependency injection

#### Cons:
* Doesn't prove that your component will receive the store in the way it's *really* being provided (with Context)
* Makes assumptions about how `@inject` will discover and pass stores to your component. Could break if `mobx-react` changes.

```js
const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
```

### 2. Using a real `<Provider />` from `mobx-react`

Just as our application uses `<Provider />` to make stores available to child components, we can also use it in our tests. 

#### Pros:
* Makes no assumptions about how MobX uses Context to pass stores to components. Should be less fragile as it updates along with MobX.
* Reflects more accurately how stores are passed around in the real application.



#### Normal usage:
```js
import { Provider } from "mobx-react";
import { render, fireEvent, wait } from '@testing-library/react';

const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
```



## Linting & Formatting

* [ESLint](https://eslint.org/) 
