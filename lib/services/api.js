import {apiURLs} from '../config';
import getConfig from 'next/config';
import axios from 'axios';

const {publicRuntimeConfig = {}} = getConfig() || {};

const BASE_URL =  apiURLs.tools.BASE_URL;

// FETCH part
export const fetchMiniSites = async () => {
  const url = `${BASE_URL}/sites`;
  
  try {
    const res = await axios.get(url);
    return res.data;
  }
  catch (e) {
    console.error(e);
  }
};

export const fetchMiniSiteDeployedApi = async siteName => {
  const url = `${BASE_URL}/sites/current-deployed`;
  
  try {
    const res = await axios.get(url, {params: {siteName}});
    return res.data;
  }
  catch (e) {
    console.error(e);
  }
};

export const fetchLastBuildStatusApi = async name => {
  const url = `${BASE_URL}/version-status`;
  
  try {
    let res = await axios.get(url, {params: {name}});
    return res && res.data;
  }
  catch (e) {
    console.error(e);
  }
};

export const fetchMiniSiteVersionsApi = async siteName => {
  const url = `${BASE_URL}/sites/versions`;
  
  try {
    let res = await axios.get(url, {params: {siteName}});
    return res && res.data;
  }
  catch (e) {
    console.error(e);
  }
};

// POST part
export const setExperience = async (formData, onUploadProgress) => {
  try {
    return axios.post('/api/upload', formData, {...onUploadProgress});
  }
  catch (e) {
    console.error(e);
  }
};

