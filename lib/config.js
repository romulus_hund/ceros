const apiURLs = {
  local: {
    BASE_URL: 'http://localhost:3000/api'
  },
  staging: {
    BASE_URL: 'https://ceros-publisher.staging.theculturetrip.com/api'
  },
  tools: {
    BASE_URL: 'https://ceros-publisher.tools.theculturetrip.com/api'
  }
};

const bucketName = 'ceros-publisher';

module.exports = {apiURLs, bucketName};
