const http = require('http');
const express = require('express');
const cookieParser = require('cookie-parser');
// const bodyParser = require('body-parser');

module.exports = function createServer() {
  const app = express();

  // app.use(bodyParser({limit: '50mb'}));
  // app.use(bodyParser.json({limit: '50mb'}));
  // app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

  const server = http.createServer(app);

  app.use(express.static('static'));
  app.use(cookieParser());

  return { app, server };
};
