const
  fs = require('fs'),
  OUTPUT_PATH = 'lib/server/dest',
  {sendError} = require('./ZIPhandler/sendError');

module.exports = createFormData = async (res, data, error, files, formData, jenkins) => {
  const
    arrayJnk = Array.from(JSON.parse(jenkins).parameter),
    updateJenkins = {};
  
  updateJenkins['parameter'] = arrayJnk.map(param => {
    if (param.name === 'SITE') {
      return {'name': 'SITE', 'value': data.name};
    }
    if (param.name === 'VERSION') {
      if (data.newSite) {
        return {'name': 'VERSION', 'value': '1.0.0'};
      }
      else if (data.versions) {
        return {'name': 'VERSION', 'value': `${data.versions.length + 1}.0.0`};
      }
    }
    return param;
  });
  
  try {
    formData.append('file0', fs.createReadStream(files.file.path));
    formData.append('json', JSON.stringify(updateJenkins));
    fs.writeFile(`${OUTPUT_PATH}/config.json`,
      JSON.stringify(data),
      (err) => {
        if (err) {
          console.error(err);
          return null;
        }
        formData.append('file1', fs.createReadStream(`${OUTPUT_PATH}/config.json`));
        return formData;
      });
  }
  catch (e) {
    console.log('createFormData', e);
    return sendError(res,
      400,
      `CreateFormData: ${e && e.response ? e.response : 'undefined'}`);
  }
};
