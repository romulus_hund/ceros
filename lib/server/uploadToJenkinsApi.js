const axios = require('axios');

module.exports = uploadToJenkinsApi = async (formData) => {
  const formHeaders = formData.getHeaders();
  const token = Buffer.from(`jenkins@theculturetrip.com:117d0c4c7e0bcfe84aecc726aa42abc179`, 'utf8')
    .toString('base64');
  try {
    return axios({
      method: 'post',
      url: 'http://jenkins.internal.tools.theculturetrip.com/job/ceros-trigger/build',
      data: formData,
      maxContentLength: 100000000,
      maxBodyLength: 1000000000,
      headers: {
        'Access-Control-Allow-Headers': 'Authorization',
        Authorization: `Basic ${token}`,
        ...formHeaders
      },
    });
  }
  catch (error) {
    if (error) {
      console.log('--response error: ', error.message);
      return {error: `Jenkins Error: ${error.message}`};
    }
    console.log('--error mesSage', error.response);
    return {error: `Jenkins Error: ${error.response}`};
  }
};
