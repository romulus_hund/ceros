const
  FormData = require('form-data'),
  {IncomingForm} = require('formidable'),
  dc = require('./ZIPhandler/decompress'),
  uniqId = require('uniqid'),
  createFormData = require('./createFormData'),
  {sendError} = require('./ZIPhandler/sendError'),
  uploadToJenkinsApi = require('./uploadToJenkinsApi');

var counter = 0;
module.exports = function upload(req, res) {
  const form = new IncomingForm();
  
  form.parse(req, (error, fields, files) => {
      if (error) {
        res.sendStatus(500);
      }
      else if (!fields ||
        !fields.environment ||
        !fields.version ||
        !fields.jenkins ||
        !fields.rollback ||
        fields.currentSiteName == null) {
        return sendError(res, 400, 'Error: custom property required');
      }
    // --------------
      let data = null;
      let formData = new FormData();
      
      if (fields.name) {
        Object.assign(data,
          {id: uniqId()},
          {name: fields.name},
          {version: fields.version}
        );
        
        formData.append('json', fields.jenkins);
        (async () => {
          try {
            await createFormData(res, data, error, files, formData);
          }
          catch (error) {
            return sendError(res, 500, `jenkinsHandling Error: ${error.message}`);
          }
          
          try {
            await uploadToJenkinsApi(formData);
            return res.status(200).send({data});
          }
          catch (error) {
            sendError(res, 500, `UploadToJenkinsApi error: ${error.message}`);
          }
        })();
      }
      else {
        (async () => {
          if (fields.rollback === 'false') {
            try {
              if (counter > 0) {
                counter = 0;
                return res.status(200).send({...data});
              }
              counter++;
              data = await dc(res, error, fields, files, req.headers.origin);
            }
            catch (error) {
              return sendError(res, 400, `${error.message}`);
            }
            try {
              await createFormData(res, data, error, files, formData, fields.jenkins);
            }
            catch (error) {
              return sendError(res, 400, `createFormData Error: ${error.message}`);
            }
          }
          try {
            if (!data) {
              formData.append('json', fields.jenkins);
              data = {};
              let r, name = '';
              const j = JSON.parse(fields.jenkins);
              if (j && j.parameter) {
                r = j.parameter.find(obj => obj.name === 'SITE');
                if (r) {
                  name = r.value;
                  Object.assign(data,
                    {siteId: fields.siteId},
                    {name},
                    {version: fields.version}
                  );
                }
              }
              formData.append('json', fields.jenkins);
            }
            await uploadToJenkinsApi(formData);
            counter = 0;
            return res.status(200).send({...data});
          }
          catch (error) {
            return sendError(res, 500, `UploadToJenkinsApi error: ${error.message}`);
          }
        })();
      }
    }
  );
};
