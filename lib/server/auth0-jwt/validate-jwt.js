const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa');
const authConfig = require('./config');

module.exports = function validateJwt(token, done) {
  const client = jwksClient({
    jwksUri: `https://${authConfig.domain}/.well-known/jwks.json`
  });

  function getKey(header, callback) {
    client.getSigningKey(header.kid, (err, key) => {
      var signingKey = key.publicKey || key.rsaPublicKey;
      callback(null, signingKey);
    });
  }

  jwt.verify(token, getKey, (err, user) => {
    if (err) {
      return done(err, null);
    }
    done(null, user);
  });
};
