const authConfig = require('./config');

module.exports.clearJwtCookie = function clearJwtCookie(res) {
  res.cookie(authConfig.jwtCookieName, '', { maxAge: -1 });
};
module.exports.redirectToLogin = (req, res) => res.redirect(authConfig.authURL);
