const addLoginRoutes = require('./add-login-routes');
const authenticate = require('./authenticate');
const { redirectToLogin } = require('./helpers');

module.exports.addLoginRoutes = addLoginRoutes;
module.exports.authenticate = authenticate;
module.exports.redirectToLogin = redirectToLogin;
