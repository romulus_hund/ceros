const url = require('url');
const { isNextRequest } = require('../helpers');
const authConfig = require('./config');
const validateJwt = require('./validate-jwt');
const { clearJwtCookie } = require('./helpers');

module.exports = function authenticate(config = {}) {
  return (req, res, next) => {

    const onFail = config.onFail || next;
    const token = req.cookies[authConfig.jwtCookieName];
    const { pathname } = url.parse(req.originalUrl);

    if (
      isNextRequest(req) ||
      pathname === authConfig.authURL ||
      pathname === authConfig.callbackURL
    ) {
      return next();
    }

    if (!token) {
      return onFail(req, res, next);
    }

    validateJwt(token, (err, user) => {

      if (err) {
        clearJwtCookie(res);
        return onFail(req, res, next);
      }
      req.user = user;
      next();
    });
  };
};
