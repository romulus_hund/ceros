const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const authConfig = require('./config');
const { clearJwtCookie } = require('./helpers');
const validateJwt = require('./validate-jwt');

const dev = process.env.NODE_ENV !== 'production';

module.exports = function addAuth0JWTLogin(app) {
  app.use(passport.initialize());

  passport.use(
    new Auth0Strategy(
      {
        domain: authConfig.domain,
        clientID: authConfig.clientId,
        clientSecret: authConfig.clientSecret,
        callbackURL: 'dummy-value',
        scope: authConfig.scope,
        state: false
      },
      (accessToken, refreshToken, extraParams, profile, done) => {
        return done(null, profile, extraParams.id_token);
      }
    )
  );

  const authenticationHandler = options => (req, res, next) => {
    const protocol = process.env.EDGE_PROTOCOL || req.protocol;
    const host = req.get('host');
    return passport.authenticate('auth0', {
      ...options,
      callbackURL: `${protocol}://${host}${authConfig.callbackURL}`
    })(req, res, next);
  };

  app.get(authConfig.authURL, authenticationHandler(), (req, res) =>
    res.redirect('/')
  );

  app.get(
    authConfig.callbackURL,
    authenticationHandler({
      session: false,
      failureRedirect: '/login'
    }),
    (req, res) => {
      if (!req.user || !req.authInfo) {
        throw new Error('user not authenticated');
      }
      validateJwt(req.authInfo, (err, user) => {
        if (err) {
          throw new Error('invalid jwt returned by auth0');
        }
        res.cookie(authConfig.jwtCookieName, req.authInfo, {
          maxAge: authConfig.jwtCookieMaxAge,
          httpOnly: true,
          secure: !dev
        });
        res.redirect('/');
      });
    }
  );

  app.get('/logout', (req, res) => {
    clearJwtCookie(res);
    res.redirect('/');
  });
};
