const uniqId = require('uniqid');
const exec = require('child_process').exec;
const config = require('../config');

const {bucketName} = config;

module.exports = async function getSitesList(req, res) {
  try {
    exec(`php ${__dirname}/php-s3/getListOfObjects.php ${bucketName}`, (error, stdout, stderr) => {
      if (error) {
        console.log(`----error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`---stderr: ${stderr}`);
        return;
      }
      
      const parsedData = JSON.parse(stdout);
      
      const filteredConfigs = parsedData && parsedData.filter(item => {
        if (!item.Key.includes('/production/')
          && !item.Key.includes('/staging/')
          && item.Key.includes('/')) {
          return item.Key;
        }
      });
      
      const siteNamesArray = filteredConfigs && filteredConfigs
        .map(item => {
          return item.Key.split('/')[0];
        });
      
      const siteNames = [...new Set(siteNamesArray)];
      
      const paths = filteredConfigs && filteredConfigs.map(item => {
        return item.Key;
      });
      
      exec(`php ${__dirname}/php-s3/getConfigs.php ${paths}`, (error, stdout, stderr) => {
        
        if (error) {
          console.log(`----error: ${error.message}`);
          return;
        }
        if (stderr) {
          console.log(`---stderr: ${stderr}`);
          return;
        }
        
        let miniSitesList = [];
        
        const parsed = JSON.parse(stdout);
        
        if (!parsed) {
          return res.json([]);
        }
        
        siteNames.map(name => {
          let newSiteObj = {
            name: name,
            siteId: uniqId(),
          };
          parsed.map(item => {
            if (item && item.name === name) {
              newSiteObj.siteId = item.siteId;
              newSiteObj.title = item.title;
            }
          });
          miniSitesList.push(newSiteObj);
        });
        res.send(miniSitesList);
        
      });
      
    });
  }
  catch (e) {
    console.log('--e: ', e);
    throw new Error(`Error: ${e.message}`);
  }
};
