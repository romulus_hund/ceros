const path = require('path');
const next = require('next');
const express = require('express');
const cors = require('cors');
const createServer = require('./http-server/create-server');
const {
  addLoginRoutes,
  authenticate,
  redirectToLogin
} = require('./auth0-jwt');
const bodyParser = require('body-parser');
const dev = process.env.NODE_ENV !== 'production';
const upload = require('./upload');
const getSitesList = require('./getSitesList');
const getSiteVersions = require('./getSiteVersions');
const fetchMiniSiteDeployed = require('./fetchMiniSiteDeployed');
const nextApp = next({dev, dir: path.resolve(__dirname, '..', 'app')});
const nextHandler = nextApp.getRequestHandler();
const port = 3000;
const axios = require('axios');

nextApp.prepare().then(() => {
  const {app, server} = createServer();
  
  app.use((req, res, next) => {
    req.port = port;
    next();
  });
  app.use(cors());
  app.use(express.static(__dirname + '/../app/public'));
  addLoginRoutes(app);
  app.use('/__/health', (req, res) => {
    res.send('OK');
  });
  app.use(bodyParser.json({limit: '150mb'}));
  app.use(bodyParser.urlencoded({
    limit: '150mb',
    extended: true,
    parameterLimit: 50000
  }));
  

  
  app.get('*', nextHandler);
  
  app.get('/api/sites/current-deployed/', fetchMiniSiteDeployed);
  app.get('/api/sites/', getSitesList);
  app.get('/api/sites/versions', getSiteVersions);
  app.get('/api/version-status', async (req, res) => {
    const token = Buffer.from(`jenkins@theculturetrip.com:117d0c4c7e0bcfe84aecc726aa42abc179`, 'utf8')
      .toString('base64');
    let siteName = '';
    if (req.query) {
      siteName = req.query.name;
    }
    try {
      const resp = await axios({
        headers: {
          'Access-Control-Allow-Headers': 'Authorization',
          Authorization: `Basic ${token}`
        },
        method: 'get',
        url: `https://jenkins.internal.tools.theculturetrip.com/job/ceros-deployment-${siteName}/lastBuild/api/json`,
      });// ceros-trailsoftheunexpected
      res.status(200).send({data: resp.data});
    }
    catch (error) {
      console.log(error.message);
      res.status(400).send({message: `Ping failed ${error.message}`});
    }
  });
  app.post('/api/upload', upload);
  
  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
