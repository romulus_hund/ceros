/**
 * Loads the configuraton of the service from either environment variables or from the secret manager.
 *
 * It will store them as a singleton so that the configuration is loaded only once.
 */
const SINGLETON_KEY = Symbol.for('ceros-publisher');
const AWS = require('aws-sdk');
const util = require('util');
const region = 'us-east-1';
// import { logging } from 'datadog-node-utils'

const secretPath =
  process.env.SERVICE_SECRET_PATH ||
  process.env.ENVIRONMENT + '/' + process.env.SERVICE_NAME;

const client = new AWS.SecretsManager({
  region: region
});

// interface ServiceConfig {
//   ClientId: string
//   ClientSecret: string
//   UserPoolId: string
//   TemporaryPassword: string
// }

const getServiceConfig = async function () {
  if (!global[SINGLETON_KEY]) {
    if (!secretPath) {
      global[SINGLETON_KEY] = {
        ClientId: process.env.COGNITO_CLIENT_ID,
        ClientSecret: process.env.COGNITO_CLIENT_SECRET,
        UserPoolId: process.env.COGNITO_USER_POOL,
        TemporaryPassword: process.env.COGNITO_TEMPORARY_PASSWORD
      };
    }
    else {
      console
        .info('Loading configuration from secret manager at ' + secretPath);
      const getSecretValueAsync = util
        .promisify(client.getSecretValue)
        .bind(client);
      
      await getSecretValueAsync({SecretId: secretPath})
        .then(function (data) {
          if ('SecretString' in data) {
            const secrets = JSON.parse(data.SecretString);
            
            global[SINGLETON_KEY] = {
              ClientId: secrets['COGNITO_CLIENT_ID'],
              ClientSecret: secrets['COGNITO_CLIENT_SECRET'],
              UserPoolId: secrets['COGNITO_USER_POOL'],
              TemporaryPassword: secrets['COGNITO_TEMPORARY_PASSWORD']
            };
            console
              .info('Loaded configuration from secret manager');
          }
          else {
            throw new Error('Binary secrets not supported');
          }
        })
        .catch(function (err) {
          console
            .error('Could not load configuration. Received ' + err);
          
          throw err;
        });
    }
  }
  return global[SINGLETON_KEY];
};

module.exports = getServiceConfig;
