<?php

namespace culturetrip;

use Aws\Exception\AwsException;
use Aws\SecretsManager\SecretsManagerClient;
use Aws\S3\S3Client;
use \stdClass;

class AwsCredentials
{
    public function __construct($profile_name = "default", $region = "us-east-1")
    {
        $params = [
            'version' => 'latest', // ''2017-10-17',
            'region' => $region,
        ];

        if (!empty($profile_name)) {
            $params['profile']= $profile_name;
        }


        $this->client = new S3Client($params);
    }

    public function get_list_of_s3_objects($bucket_name) {

            $result = $this->client->listObjectsV2(['Bucket' => $bucket_name]);


            return( $result);

    }


    public function getConfigs ($myString){

     if(!$myString || $myString === 'null'){
         return null;
      }

     /* echo file_put_contents ('daata2.txt', $arr); */

      $paths = explode(',', $myString);

        $results = array_map(function($element) {

                $s3Object = $this->client->getObject(array(
                   'Bucket' => 'ceros-publisher',
                    'Key'    => $element
                ));

                $bodyAsString = $s3Object['Body'];

                return json_decode($bodyAsString, true);

            },
            $paths
        );

        return $results;

    }

}


