<?php

namespace culturetrip;

require_once 'vendor/autoload.php';
require 'aws-credentials.php';


$myString = isset($argv[1]) ? $argv[1] : null;


$sitesNames = array();
if (strpos($myString, ',') !== false) {
    $sitesNames = explode(',', $myString);
/*     echo file_put_contents ('daata1.txt', $sitesNames) ;*/
}else{
    array_push($sitesNames, $myString);
/*     echo file_put_contents ('daata2.txt', $sitesNames); */
}

 // TODO: check if one site name

$env_var = "ENVIRONMENT";
$env = getenv($env_var);
$env = !empty($env) ? $env : "local";

$aws_profiles = [
    "local" => "default",
    "local-dev" => "dev",
    "development" => null,
    "staging" => null,
    "tools" => null,
    "production" => null,
];
$aws_profile_name = $aws_profiles[$env];

$secret_prefixes = [
    "local" => "production",
    "local-dev" => "development",
    "development" => $env,
    "staging" => $env,
    "tools" => $env,
    "production" => $env,
];
$secret_prefix = $secret_prefixes[$env];

// connect to AWS Secret Manager
$credentials = null;
try {
    $credentials = new AwsCredentials($aws_profile_name);
} catch (\Exception $ex) {
    throw $ex;
}


$configs = null;
try {

    if (empty($sitesNames)) {
        throw new \Exception("Empty sitesNames retrieved");
    }

   $configObjects = $credentials->getConfigs($myString);

    echo json_encode($configObjects);

} catch (\Exception $ex) {
    throw $ex;
}






