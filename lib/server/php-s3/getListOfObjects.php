<?php

namespace culturetrip;

require_once 'vendor/autoload.php';
require 'aws-credentials.php';

$bucket_name = $argv[1];

$env_var = "ENVIRONMENT";
$env = getenv($env_var);
$env = !empty($env) ? $env : "local";

$aws_profiles = [
    "local" => "default",
    "local-dev" => "dev",
    "development" => null,
    "staging" => null,
    "tools" => null,
    "production" => null,
];

$aws_profile_name = $aws_profiles[$env];

$secret_prefixes = [
    "local" => "production",
    "local-dev" => "development",
    "development" => $env,
    "staging" => $env,
    "tools" => $env,
    "production" => $env,
];

$secret_prefix = $secret_prefixes[$env];

// connect to AWS Secret Manager
$credentials = null;

try {
    $credentials = new AwsCredentials($aws_profile_name);
} catch (\Exception $ex) {
    throw $ex;
}


$s3Objects = null;
try {
    $s3Objects = $credentials->get_list_of_s3_objects($bucket_name);

    if (empty($s3Objects)) {
        throw new \Exception("Empty secrets retrieved");
    }

    echo json_encode($s3Objects['Contents']);

} catch (\Exception $ex) {
    throw $ex;
}






