module.exports.sendError = function sendError(res, status, message) {
  return res.status(status).json(
    {
      error:
        {
          code: status,
          message
        }
    });
};

