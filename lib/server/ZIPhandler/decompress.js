const
  fs = require('fs'),
  util = require('util'),
  axios = require('axios'),
  readFileAsync = util.promisify(fs.readFile),
  OUTPUT_PATH = 'lib/server/dest',
  cheerio = require('cheerio'),
  decompress = require('decompress'),
  decompressTargz = require('decompress-targz'),
  rimraf = require('rimraf'),
  {sendError} = require('./sendError');

module.exports = dc = async (res, error, fields, files, baseUrl) => {
  try {
    let zip = await decompress(files.file.path, OUTPUT_PATH, {
      plugins: [
        decompressTargz()
      ],
      filter: file => {
        return file.path.includes('index.html');
      }
    });
    try {
      let
        data = {},
        result = null;
      const
        str = await readFileAsync(`${OUTPUT_PATH}/${zip[0].path}`),
        $ = cheerio.load(str.toString());
      
      $('script').map(function (i, el) {
        if ($(el).toString().includes('issue:')) {
          result = $(el);
        }
      });
      // result = null;
      if (!result) {
        rimraf(OUTPUT_PATH, function () {
          console.log('Decompress succeeded! Output folder was deleted');
        });
        return sendError(res, 400, `Warning: required Ceros properties don't exist`);
      }
      const
        regex1 = /issueAliasSlug:\s*('([^}]+)')/gm,
        issueAliasSlug = regex1.exec(result.toString())[2],
        regex = /issue:\s*({([^}]+)})/gm,
        found = regex.exec(result.toString())[1],
        {lastModifiedDate, creationDate, title, id} = JSON.parse(found);
      
      let
        firstVersion = null,
        tempName = null,
        getVersions = null;
      
      if (!id || !issueAliasSlug) {
        rimraf(OUTPUT_PATH, function () {
          console.log('Output folder was deleted');
        });
        return sendError(res, 400, `Warning: required Ceros properties don't exist`);
      }
      // TODO: temporary part:
      if (JSON.parse(fields.sitesList).length === 0 && issueAliasSlug === 'footer-pages-test') {
        tempName = 'ceros-trailsoftheunexpected';
      }
      // --------------------
      if (JSON.parse(fields.sitesList).length === 0) {
        firstVersion = fields.version;
      }
      else if (JSON.parse(fields.sitesList).length > 0 &&
        fields.currentSiteName.length > 0 &&
        issueAliasSlug !== fields.currentSiteName) {
        // TODO: temporary part:
        if (fields.currentSiteName === 'ceros-trailsoftheunexpected'
          && issueAliasSlug === 'footer-pages-test') {
          tempName = 'ceros-trailsoftheunexpected';
        }
        // --------------------
        else {
          if (issueAliasSlug === 'footer-pages-test') {
            tempName = 'ceros-trailsoftheunexpected';
          }
          const isSite = JSON.parse(fields.sitesList).some(site => {
            if (tempName) {
              return site.name === tempName;
            }
            return site.name === issueAliasSlug;
          });
          if (!isSite) {
            firstVersion = '1.0.0';
          }
          else {
            try {
              getVersions = await axios.get(`${baseUrl}/api/sites/versions`,
                {params: {siteName: tempName || issueAliasSlug}});
              if (getVersions && getVersions.data) {
                firstVersion = `${getVersions.data.length + 1}.0.0`;
              }
            }
            catch (e) {
              console.log('==========> isSite Error------', e);
            }
          }
        }
      }
      
      Object.assign(data,
        {siteId: id},
        {name: tempName || issueAliasSlug},
        {title},
        {creationDate},
        {lastModifiedDate},
        {version: firstVersion || fields.version},
        {newSite: firstVersion && firstVersion === '1.0.0'},
        {versions: getVersions ? getVersions.data : null},
      );
      
      rimraf(OUTPUT_PATH, function () {
        console.log('Decompress succeeded! Output folder was deleted');
      });
      
      return data;
    }
    catch (e) {
      /*
        1. if archive without "index.html"
        2. if problem with reading file "index.html"
        3. if received some another format instead TAR.GZ
      */
      return sendError(res, 400, `ZIP Error: This archive is not valid. Please choose another file`);
    }
  }
  catch (error) {
    // ex. "Decompress Error: Invalid tar header.js.
    //      Maybe the tar is corrupted or it needs to be gunzipped?"
    console.log(`Decompress ${error}`);
    return sendError(res, 400, `Decompress ${error}`);
  }
};
