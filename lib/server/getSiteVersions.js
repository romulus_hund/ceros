const exec = require('child_process').exec;
const config = require('../config');

const {bucketName} = config;

module.exports = async function getSiteVersions(req, res) {
  const {siteName} = req.query;
  
  try {
    
    exec(`php ${__dirname}/php-s3/getListOfObjects.php ${bucketName}`, (error, stdout, stderr) => {
      if (error) {
        console.log(`----error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`---stderr: ${stderr}`);
        return;
      }
      
      const parsedData = JSON.parse(stdout);
  
      if(!parsedData){
        return res.send([])
      }
      
      const filteredConfigs = parsedData.filter(item => {
        if (!item.Key.includes('/production/')
          && !item.Key.includes('/staging/')
          && item.Key.includes(siteName)
          && item.Key.includes('config.json')
        ) {
          return item.Key;
        }
      });
      
      const paths = filteredConfigs.map(item => {
        return item.Key;
      });
      
      exec(`php ${__dirname}/php-s3/getConfigs.php ${paths}`, (error, stdout, stderr) => {
        if (error) {
          console.log(`----error: ${error.message}`);
          return;
        }
        if (stderr) {
          console.log(`---stderr: ${stderr}`);
          return;
        }
        
        const parsed = JSON.parse(stdout);
  
        if(!parsed){
          return res.send([])
        }
        
        const versions = parsed.filter(config => config && config.name === siteName);
        
        res.json([...versions]);
        
      });
      
    });
  }
  catch (e) {
    console.log('--e: ', e);
    throw new Error(`Error: ${e.message}`);
  }
};
