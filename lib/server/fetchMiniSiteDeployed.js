const {exec} = require('child_process');
const config = require('../config');

const {bucketName} = config;

module.exports = async function fetchMiniSiteDeployed(req, res) {
  
  const {siteName} = req.query;
  
  const encTemplate = {
    version: null,
    status: null
  };
  
  let deployedVersions = {
    production: [],
    staging: []
  };
  
  exec(`php ${__dirname}/php-s3/getListOfObjects.php ${bucketName}`, (error, stdout, stderr) => {
    if (error) {
      console.log(`----error: ${error.message}`);
      return;
    }
    if (stderr) {
      console.log(`---stderr: ${stderr}`);
      return;
    }
    
    const parsedData = JSON.parse(stdout);
  
    if (!parsedData) {
      return res.json({});
    }
    
    const filteredConfigs = parsedData.filter(item => {
      if (item.Key.includes('/production/')
        && !item.Key.includes('/staging/')
        && item.Key.includes(siteName)
        && item.Key.includes('config.json')
      ) {
        return item.Key;
      }
    });
    
    const paths = filteredConfigs.map(item => {
      return item.Key;
    });
    
    exec(`php ${__dirname}/php-s3/getConfigs.php ${paths}`, (error, stdout, stderr) => {
      
      if (error) {
        console.log(`----error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`---stderr: ${stderr}`);
        return;
      }
      
      const parsed = JSON.parse(stdout);
      if (parsed) {
        const versions = parsed.filter(config => config && config.name === siteName);
        deployedVersions.production[0] = {...encTemplate, version: versions && versions[0] ? versions[0].version : null};
      }
      
      const filteredConfigs = parsedData.filter(item => {
        if (!item.Key.includes('/production/')
          && item.Key.includes('/staging/')
          && item.Key.includes(siteName)
          && item.Key.includes('config.json')
        ) {
          return item.Key;
        }
      });
      
      const paths = filteredConfigs.map(item => {
        return item.Key;
      });
      
      exec(`php ${__dirname}/php-s3/getConfigs.php ${paths}`, (error, stdout, stderr) => {
        if (error) {
          console.log(`----error: ${error.message}`);
          return;
        }
        if (stderr) {
          console.log(`---stderr: ${stderr}`);
          return;
        }
        
        const parsed = JSON.parse(stdout);
        
        if (parsed) {
          const versions = parsed.filter(config => config && config.name === siteName);
          deployedVersions.staging[0] = {...encTemplate, version: versions && versions[0] ? versions[0].version : null};
        }
        
        res.json({...deployedVersions});
        
      });
      
    });
    
  });
  
};
