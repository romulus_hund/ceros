module.exports.isNextRequest = function isNextRequest(req) {
  return req.originalUrl.slice(0, 7) === '/_next/';
};
