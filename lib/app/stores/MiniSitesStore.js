import {action, computed, observable, runInAction, toJS} from 'mobx';
import {fetchLastBuildStatusApi, fetchMiniSiteDeployedApi, fetchMiniSites, fetchMiniSiteVersionsApi} from '../../services/api';
import CONSTANTS from '../constants';

const {STATUS, ENV} = CONSTANTS;

export default class MiniSitesStore {
  @observable loadMiniSitesError = null;
  @observable isLoadingMiniSites = false;
  @observable miniSitesList = [];
  @observable miniSiteVersions = [];
  @observable miniSiteDeployed = {};
  @observable currentSiteId = null;
  @observable currentSiteName = null;
  @observable isPending = false;
  @observable jenkinsJobStatus = null;
  
  @action
  async fetchMiniSitesData() {
    this.isLoadingMiniSites = true;
    
    try {
      const miniSites = await fetchMiniSites();
      
      runInAction(() => {
        this.miniSitesList = miniSites;
        this.isLoadingMiniSites = false;
      });
    }
    catch (error) {
      console.error(`Failed to load mini-sites. error: ${error}`, error);
      runInAction(() => {
        this.loadMiniSitesError = error;
        this.isLoadingMiniSites = false;
      });
    }
    finally {
      runInAction(() => {
        this.loadMiniSiteData();
      });
    }
  }
  
  @action
  setCurrentSiteId = async id => {
    this.currentSiteId = id;
    this.miniSiteVersions = [];
    this.miniSiteDeployed = {};
    this.jenkinsJobStatus = null;
    const name = this.convertedMiniSitesList &&
      this.convertedMiniSitesList.find(site => site.siteId === id).name;
    this.isLoadingMiniSites = true;
    await Promise.all([
      this._fetchMiniSiteVersions(name),
      this._fetchMiniSiteDeployed(name),
      this._fetchLastBuildStatus(name)
    ]);
    runInAction(() => {
      this.currentSiteName = name;
      this.isLoadingMiniSites = false;
      this._updateMiniSiteDeployed();
    });
  };
  
  @action
  initSiteData = async () => {
    this.isLoadingMiniSites = true;
    await Promise.all([
      this._fetchMiniSiteVersions(),
      this._fetchMiniSiteDeployed(),
      this._fetchLastBuildStatus()
    ]);
    runInAction(() => {
      this.isLoadingMiniSites = false;
      this._updateMiniSiteDeployed();
    });
  };
  
  async _fetchMiniSiteVersions(name) {
    this.isLoadingMiniSites = true;
    try {
      const versions = await fetchMiniSiteVersionsApi(name || this.currentSiteName);
      
      runInAction(() => {
        this.miniSiteVersions = versions;
        this.isLoadingMiniSites = false;
      });
    }
    catch (error) {
      console.error(`Failed to load mini-site versions. error: ${error}`, error);
      runInAction(() => {
          this.miniSiteVersions = [];
          this.loadMiniSitesError = error;
          this.isLoadingMiniSites = false;
        }
      );
    }
  }
  
  async _fetchMiniSiteDeployed(name) {
    try {
      const deployed = await fetchMiniSiteDeployedApi(name || this.currentSiteName);
      
      runInAction(() => {
        this.miniSiteDeployed = deployed;
      });
      
    }
    catch (error) {
      console.error(`Failed to load mini-site versions. error: ${error}`, error);
      runInAction(() => {
        this.miniSiteDeployed = {};
        this.loadMiniSitesError = error;
        this.isLoadingMiniSites = false;
      });
    }
  }
  
  async _fetchLastBuildStatus(name) {
    try {
      const status = await fetchLastBuildStatusApi(name || this.currentSiteName);
      
      runInAction(() => {
        let data = [];
        if (status !== undefined && status.data) {
          const {result, actions, id, url} = status.data;
          const parameters = actions?.find(action => action?.parameters?.length > 0)?.parameters;
          data = parameters?.reduce((res, val) => {
            return {...res, [val.name]: val.value};
          }, {});
          data.result = result;
          data.url = url;
          data.id = id; // id: '208' - string
          this.jenkinsJobStatus = data;
        }
      });
    }
    catch (error) {
      console.error(`Failed to load mini-site status. error: ${error}`, error);
      runInAction(() => {
        this.jenkinsJobStatus = null;
        this.loadMiniSitesError = error;
        this.isLoadingMiniSites = false;
      });
    }
  }
  
  @computed
  get convertedMiniSitesList() {
    if (this.miniSitesList) {
      return toJS(this.miniSitesList);
    }
    return [];
  }
  
  @computed
  get convertMiniSiteVersions() {
    if (this.miniSiteVersions) {
      return toJS(this.miniSiteVersions);
    }
    return [];
  }
  
  @action
  async loadMiniSiteData() {
    if (this.convertedMiniSitesList && this.convertedMiniSitesList.length > 0) {
      this.currentSiteId = this.convertedMiniSitesList[0].siteId;
      this.currentSiteName = this.convertedMiniSitesList[0].name;
    }
  }
  
  @action
  setStatus(status) {
    this.siteStatus = status;
  }
  
  @action.bound
  async poling(setExperienceRes, currLastBuildId) {
    let interval = await setInterval(async () => {
      try {
        this.jenkinsJobStatus = null;
        await this._fetchLastBuildStatus(setExperienceRes && setExperienceRes.name);

        if(!this.jenkinsJobStatus){
          clearInterval(interval);
          this._setNewVersionResultFromCatch(setExperienceRes);
        }
        else if ((currLastBuildId && currLastBuildId !== this.convertedStatus.id) && this.convertedStatus.result) {
          clearInterval(interval);
          this.setNewVersionResult(this.convertedStatus);
        }
      }
      catch (e) {
        console.log('======> inside poling error - ', e);
        clearInterval(interval);
        this._setNewVersionResultFromCatch(setExperienceRes);
      }
      
    }, 4000);
  }
  
  _setNewVersionResultFromCatch(data) {
    let obj, envR = null, ver = '';
    if (!data) {
      if (this.convertedMiniSiteDeployed.production.length) {
        envR = ENV.productionKey;
        ver = this.convertedMiniSiteDeployed.production[0].version;
      }
      else {
        envR = ENV.stagingKey;
        ver = this.convertedMiniSiteDeployed.staging[0].version;
      }
      
      obj = {
        ENVIRONMENT: envR,
        result: STATUS.failed,
        version: ver
      };
    }
    else {
      obj = {
        ENVIRONMENT: data.environment,
        result: STATUS.failed,
        version: data.version
      };
    }
    this.setNewVersionResult(obj);
  }
  
  @action.bound
  async addNewVersion(data) {
    if (data.newSite) {
      this.miniSitesList.push({
        name: data.name,
        siteId: data.siteId,
        title: data.title
      });
      this.currentSiteId = data.siteId;
      this.currentSiteName = data.name;
      this.miniSiteVersions = [];
      this.miniSiteVersions.push(data);
      this.miniSiteDeployed = {[ENV.productionKey]: [], [ENV.stagingKey]: []};
      this.miniSiteDeployed[data.environment].push({
        version: data.version,
        status: STATUS.pending
      });
     }
    else if(data.versions && data.versions.length > 0){
      this.currentSiteId = data.siteId;
      this.currentSiteName = data.name;
      this.miniSiteVersions = data.versions;
      this.miniSiteVersions.push(data);
      this.miniSiteDeployed = {[ENV.productionKey]: [], [ENV.stagingKey]: []};
      this.miniSiteDeployed[data.environment].push({
        version: data.version,
        status: STATUS.pending
      });
    }
    else {
      if (this.miniSiteVersions) {
        this.miniSiteVersions.push(data);
      }
      if (this.miniSiteDeployed && this.miniSiteDeployed[data.environment]) {
        this.miniSiteDeployed[data.environment].push({
          version: data.version,
          status: STATUS.pending
        });
      }
    }
    await this.lastBuildPoling(data);
  }
  
  @action.bound
  async lastBuildPoling(data) {
    if (this.convertedStatus.id) {
      try {
        await this.poling(data, this.convertedStatus.id);
      }
      catch (e) {
        this.setNewVersionResult({
          ENVIRONMENT: data.environment,
          result: STATUS.failed,
          version: data.version
        });
      }
    }
    else {
      try {
        await this._fetchLastBuildStatus();
        const currLastBuildId = this.convertedStatus && this.convertedStatus.id;
        await this.poling(data, currLastBuildId);
      }
      catch (e) {
        this._setNewVersionResultFromCatch(data);
      }
    }
  }
  
  @action.bound
  changeVersionEnvironment(data) {
    this.miniSiteDeployed[data.environment].push({
      version: data.version,
      status: STATUS.pending
    });
  }
  
  @action.bound
  setNewVersionResult(res) {
    const {production, staging} = this.convertedMiniSiteDeployed;
    if (production.length > 1 || staging.length > 1) {
      if (production.length > 1) {
        this.miniSiteDeployed.production.shift();
        this.miniSiteDeployed.production[0].status = res.result;
      }
      if (staging.length > 1) {
        this.miniSiteDeployed.staging.shift();
        this.miniSiteDeployed.staging[0].status = res.result;
      }
    }
    else {
      if (res && res.ENVIRONMENT) {
        if (this.convertedMiniSiteDeployed[res.ENVIRONMENT].length === 0) {
          this.miniSiteDeployed[res.ENVIRONMENT].push({
            version: res.version,
            status: res.result
          });
        }
        else {
          this.miniSiteDeployed[res.ENVIRONMENT][0].status = res.result;
        }
      }
    }
  }
  
  @computed
  get currentMiniSite() {
    return this.convertedMiniSitesList && this.convertedMiniSitesList
      .find(site => site.siteId === this.currentSiteId);
  }
  
  @computed
  get selectOptions() {
    return this.convertedMiniSitesList && this.convertedMiniSitesList.reduce((res, site) => {
      res.push({
        value: site.siteId,
        label: site.title || site.name
      });
      return res;
    }, []);
  }
  
  @computed
  get convertedMiniSiteDeployed() {
    return this.miniSiteDeployed ? toJS(this.miniSiteDeployed) : {};
  }
  
  @computed
  get convertedStatus() {
    return this.jenkinsJobStatus ? toJS(this.jenkinsJobStatus) : {};
  }
  
  _updateMiniSiteDeployed = () => {
    const {production, staging} = this.convertedMiniSiteDeployed;
    
    const jenkinsVersion = this.convertedStatus.VERSION;
    
    if (!jenkinsVersion) {
      if (production && production[0] && production[0].version) {
        this.miniSiteDeployed.production[0].status = STATUS.published;
      }
      if (staging && staging[0] && staging[0].version) {
        this.miniSiteDeployed.staging[0].status = STATUS.published;
      }
    }
    else {
      if (staging && staging[0] && staging[0].version === this.convertedStatus.VERSION
        && this.convertedStatus.ENVIRONMENT === ENV.stagingKey) {
        this.miniSiteDeployed.staging[0].status = this.convertedStatus.result;
      }
      else if (staging && staging[0] && staging[0].version) {
        this.miniSiteDeployed.staging[0].status = STATUS.published;
      }
      if (production && production[0] && production[0].version === this.convertedStatus.VERSION
        && this.convertedStatus.ENVIRONMENT === ENV.productionKey) {
        this.miniSiteDeployed.production[0].status = this.convertedStatus.result;
      }
      else if (production && production[0] && production[0].version) {
        this.miniSiteDeployed.production[0].status = STATUS.published;
      }
    }
  };
}
