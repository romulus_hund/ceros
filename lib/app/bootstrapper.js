import MiniSitesStore from './stores/MiniSitesStore';

const bootstrapper = () => {
  const miniSitesStore = new MiniSitesStore();
  
  return {miniSitesStore};
};

export default bootstrapper;
