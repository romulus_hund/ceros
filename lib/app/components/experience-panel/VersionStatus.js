import React from 'react';
import PropTypes from 'prop-types';
import {SiteStatusInfo, SiteStatusWrapper} from './experience-panel.styled';
import CONSTANTS from '../../constants';
import LabelWithStatus from './LabelWithStatus';

const {ENV} = CONSTANTS;

function VersionStatus({label, productionStatus, stagingStatus, staging, production}) {
  return (
    <SiteStatusWrapper>
      {label && <label>{label}</label>}
      <SiteStatusInfo>
        {production &&
        <LabelWithStatus
          status={productionStatus}
          text={ENV.production}
        />
        }
        {staging &&
        <LabelWithStatus
          status={stagingStatus}
          text={ENV.staging}/>
        }
      </SiteStatusInfo>
    </SiteStatusWrapper>
  );
}

VersionStatus.propTypes = {
  staging: PropTypes.bool,
  production: PropTypes.bool
};

export default VersionStatus;
