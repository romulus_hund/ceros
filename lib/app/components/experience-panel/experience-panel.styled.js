import styled from 'styled-components';
import Clear from '@material-ui/icons/Clear';
import Done from '@material-ui/icons/Done';
import Remove from '@material-ui/icons/Remove';

const VersionStatusWrapper = styled.div`
  display: flex;
  div{
    margin-right: 15px;
  }
`;

const Summary = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  justify-items: center;
  align-items: center;
  width: 100%;
`;

const Details = styled.div`
  flex-direction: column;
`;

const RedX = styled(Clear)`
  color: red;
`;

const GreenCheck = styled(Done)`
  color: green;
`;

const OrangeNo = styled(Remove)`
  color: orangered;
`;

const SiteStatusWrapper = styled.div`
  // border: 1px dashed red;
   display: flex;
   position: relative;
   flex-direction: column;
   align-items: center;
   justify-content: center;
   padding: 6px ;
   
   label{
    width: 100%;
    height: 1rem;
    color: rgba(0, 0, 0, 0.54);
    padding: 0;
    font-size: 1rem;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    line-height: 1;
    margin: 8px 0;
    }

`;

const SiteStatusInfo = styled.div`
   display: flex;
   position: relative;
   flex-direction: row;
   align-items: center;
   justify-content: center;
   margin-bottom: 0;
 `;

const LabelWithStatusWrapper = styled.div`
   display: flex;
   position: relative;
   flex-direction: row;
   align-items: center;
   justify-content: center;
`;

const LabelWithStatusText = styled.div`
    color: rgba(0, 0, 0, 0.87);
    font-size: 0.875rem;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    font-weight: 400;
    line-height: 1.0;
    letter-spacing: 0.01071em;
    margin: 8px 4px 8px 8px;
`;

const Loader = styled.div`
  height: 24px;
  width: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  
  svg{
    width: 24px;
    height: 24px;
    font-size: 24px !important;
  }
`;
export {
  Summary,
  Details,
  RedX,
  GreenCheck,
  SiteStatusWrapper,
  LabelWithStatusWrapper,
  LabelWithStatusText,
  OrangeNo,
  SiteStatusInfo,
  Loader,
  VersionStatusWrapper
};
