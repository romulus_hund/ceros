import React from 'react';
import PropTypes from 'prop-types';
import {GreenCheck, LabelWithStatusText, LabelWithStatusWrapper, Loader, RedX} from './experience-panel.styled';
import CircularProgress from '@material-ui/core/CircularProgress';
import CONSTANTS from '../../constants';

function LabelWithStatus({text, status}) {
  const {STATUS} = CONSTANTS;
  const renderStatus = () => {
    switch (status) {
    case STATUS.published:
      return (
        <GreenCheck/>
      );
    case STATUS.failed:
      return (
        <RedX/>
      );
    case STATUS.pending:
      return (
        <Loader>
          <CircularProgress style={{width: '24px', height: '24px'}}/>
        </Loader>
      );
    }
  };
  
  return (
    <LabelWithStatusWrapper data-testid={`minisite-version-status-${status}`}>
      <LabelWithStatusText>{text}</LabelWithStatusText>
      {renderStatus()}
    </LabelWithStatusWrapper>
  );
}

LabelWithStatus.propTypes = {
  text: PropTypes.string,
  status: PropTypes.string
};

export default LabelWithStatus;
