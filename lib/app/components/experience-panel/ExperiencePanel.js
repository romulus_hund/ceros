import React, {useEffect, useState} from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Divider from '@material-ui/core/Divider';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Button from '@material-ui/core/Button';
import {Details, Summary} from './experience-panel.styled';
import ConfirmationDialog from '../ConfimationDialog';
import {ActionPanel} from '../style';
import CONSTANTS from '../../constants';
import RadioSelectEnv from '../radio-select-env/RadioSelectEnv';
import VersionStatus from './VersionStatus';
import {setExperience} from '../../../services/api';

const {TEXTS, ENV, STATUS} = CONSTANTS;

const ExperiencePanel = ({
  version,
  name,
  lastModifiedDate,
  creationDate,
  siteId,
  miniSiteDeployed,
  handleChangeVersionEnv,
  handleSetNewVersionResult,
  currentSiteName,
  handleLastBuildPoling
}) => {
  
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [valueEnv, setValueEnv] = useState();
  const [isPublishDisabled, setPublishDisable] = useState(false);
  const [isRowExpanded, setRowExpanded] = useState(false);
  const [initValueEnv, setInitValueEnv] = useState({});
  const [isProduction, setProduction] = useState(false);
  const [isStaging, setStaging] = useState(false);
  const [productionStatus, setProductionStatus] = useState();
  const [stagingStatus, setStagingStatus] = useState();
  
  useEffect(() => {
    const {production, staging} = miniSiteDeployed;
    init(production);
    init(staging);
    setInitValueEnv(getInitValueEnv(production, staging));
    setStaging(staging && !!staging.find(obj => obj.version === version));
    setProduction(production && !!production.find(obj => obj.version === version));
    setStagingStatus(miniSiteDeployed.staging && miniSiteDeployed.staging[0] && miniSiteDeployed.staging[0].status);
    setProductionStatus(miniSiteDeployed.production && miniSiteDeployed.production[0] && miniSiteDeployed.production[0].status);
    
  }, []);
  
  useEffect(() => {
    if (isRowExpanded) {
      const {production, staging} = miniSiteDeployed;
      setInitValueEnv(getInitValueEnv(production, staging));
    }
  }, [isRowExpanded]);
  
  useEffect(() => {
    const {production, staging} = miniSiteDeployed;
    if (!production || !staging) return () => {
    };
   
    setInitValueEnv(getInitValueEnv(production, staging));
    setStaging(staging && !!staging.find(obj => obj.version === version));
    setProduction(production && !!production.find(obj => obj.version === version));
    
    if (production.length > 1 || staging.length > 1) {
      if (production.length > 1 && production[1].version === version) {
        setProductionStatus(production[1].status);
      }
      if (staging.length > 1 && staging[1].version === version) {
        setStagingStatus(staging[1].status);
      }
    }
    else {
      setStagingStatus(miniSiteDeployed.staging[0] && miniSiteDeployed.staging[0].status);
      setProductionStatus(miniSiteDeployed.production[0] && miniSiteDeployed.production[0].status);
    }
  }, [miniSiteDeployed]);
  
  const init = env => {
    const res = env && env.find(obj => obj.version === version);
    if (res !== undefined && res.status === STATUS.pending) {
      setRowExpanded(true);
    }
  };
  
  const getInitValueEnv = (production, staging) => {
    return {
      production: production && production[0] && production[0].version === version && production[0].status ? ENV.productionKey : null,
      staging: staging && staging[0] && staging[0].version === version && staging[0].status ? ENV.stagingKey : null
    };
  };
  
  const handleChange = event => {
    setValueEnv(event.target.value);
    const {production, staging} = miniSiteDeployed;
    
    if (event.target.value === ENV.stagingKey) {
      if (staging.length > 0 && staging[0] && staging[0].version === version) {
        setPublishDisable(true);
      }
      else {
        setPublishDisable(false);
      }
    }
    if (event.target.value === ENV.productionKey) {
      if (production.length > 0 && production[0] && production[0].version === version) {
        setPublishDisable(true);
      }
      else {
        setPublishDisable(false);
      }
    }
  };
  
  const handleOnClick = () => {
    setDialogOpen(!isDialogOpen);
  };
  
  const handlePublishConfirm = () => {
    const formData = new FormData();
    formData.append('siteId', siteId);
    formData.append('currentSiteName', currentSiteName);
    formData.append('environment', valueEnv);
    formData.append('version', version);
    formData.append('rollback', 'true');
    formData.append('jenkins', JSON.stringify({
      'parameter': [
        {'name': 'SITE', 'value': name},
        {'name': 'ENVIRONMENT', 'value': valueEnv},
        {'name': 'VERSION', 'value': version},
        {'name': 'ROLLBACK', 'value': 'true'}
      ]
    }));
    
    setExperience(formData, null)
      .then(async setExperienceRes => {
        setExperienceRes.data.environment = valueEnv;
        changeVersionEnv(setExperienceRes.data);
        lastBuildPoling(setExperienceRes.data);
      })
      .catch(e => {
        newVersionResult(STATUS.failed);
      });
    
    setDialogOpen(!isDialogOpen);
  };
  
  const lastBuildPoling = () => {
    handleLastBuildPoling();
  };
  
  const handlePublishCancel = () => {
    handleOnClick();
  };
  
  const changeVersionEnv = data => {
    handleChangeVersionEnv(data);
  };
  
  const newVersionResult = data => {
    handleSetNewVersionResult(data);
  };
  
  return (
    <ExpansionPanel
      expanded={isRowExpanded}
      onChange={(e, expanded) => setRowExpanded(expanded)}>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="panel1c-content"
        id="panel1c-header">
        <Summary data-testid={`minisite-version-header-${version}`}>
          <div data-testid={`minisite-version-${version}`} style={{textAlign: 'center'}}>
            {version}
          </div>
          <div style={{textAlign: 'center'}}>
            <VersionStatus
              production={isProduction}
              staging={isStaging}
              productionStatus={productionStatus}
              stagingStatus={stagingStatus}
              version={version}
            />
          </div>
          {/*
          TODO: not for MVP
          <div>
            <RedX/>
          </div>*/}
        </Summary>
      </ExpansionPanelSummary>
      {isRowExpanded &&
      <div data-testid={`minisite-version-details-${version}`}>
        <ExpansionPanelDetails>
          <Details>
            <div>Version: {version}</div>
            {creationDate &&
            <div>
              Created On: {new Date(creationDate).toLocaleString()}
            </div>}
            {lastModifiedDate &&
            <div>
              Last Updated On: {new Date(lastModifiedDate).toLocaleString()}
            </div>}
          </Details>
        </ExpansionPanelDetails>
        <Divider/>
        <ExpansionPanelActions>
          <ActionPanel>
            <RadioSelectEnv
              currValue={valueEnv}
              initValue={initValueEnv}
              handleChange={handleChange}
              title={'Publish to:'}
              stagingStatus={stagingStatus}
              productionStatus={productionStatus}
            />
            {isDialogOpen &&
            <ConfirmationDialog
              onConfirm={handlePublishConfirm}
              onClose={handlePublishCancel}
              open={isDialogOpen}
              content={TEXTS.confirmPublish}
              title={TEXTS.titleConfirmPublish}
            />}
            <Button
              data-testid={`minisite-version-publish-btn-${version}`}
              disabled={!valueEnv || isPublishDisabled || stagingStatus === STATUS.pending || productionStatus === STATUS.pending}
              size="small"
              color="primary"
              variant="outlined"
              onClick={handleOnClick}>
              Publish
            </Button>
          </ActionPanel>
        </ExpansionPanelActions>
      </div>}
    </ExpansionPanel>
  );
  
};

export default ExperiencePanel;

