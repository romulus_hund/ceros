import styled, {css} from 'styled-components';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import RadioSelectEnv from '../../radio-select-env/RadioSelectEnv';

const UploadWrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
    align-items: flex-start;
    text-align: left;
    overflow: hidden;
`;

const Content = styled.div`
    display: flex;
    flex-direction: row;
    padding-top: 16px;
    box-sizing: border-box;
    width: 100%;
`;

const ActionSide = styled.div`
  margin-left: 32px;
  flex-direction: column;
  align-items: flex-start;
  justify-items: flex-start;
  flex: 1;
  overflow-y: auto;
`;

const Files = styled.div`
  align-items: flex-start;
  justify-items: flex-start;
  flex: 1;
  overflow-y: auto;
  margin-bottom: 8px;
`;

const ProggressPing = styled.div`
  align-items: flex-start;
  justify-items: flex-start;
  flex: 1;
  overflow-y: auto;
  margin-bottom: 8px;
  width: 85%;
`;

const Actions = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  align-items: flex-end;
  flex-direction: column;
  margin-top: 32px;
  padding: 5px;

  button {
    margin: 0 10px 4px;
    box-sizing: border-box;
  }

  button:disabled {
    background: rgb(189, 189, 189);
    cursor: default;
  }
`;

const Title = styled.h2`
  margin-bottom: 32px;
  color: #555;
`;

const Filename = styled.span`
  margin-bottom: 8px;
  font-size: 16px;
  color: #555;
`;

const Row = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  height: auto;
  padding: 8px;
  overflow: hidden;
  box-sizing: border-box;
`;

const CheckIcon = styled(CheckCircleOutlineIcon)`
  color: #4CAF50;
  margin-left: 32px;
  opacity: 0;
  ${props => props.isvisible &&
  css`
     opacity: 1;
  `};
`;

const ProgressContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
`;

const RadioAct = styled(RadioSelectEnv)`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 50%;
`;

const Error = styled.div`
  color: red;
`;

const Warning = styled.div`
  color: #ff9800;
`;

const Form = styled.div`
    margin: 0 0 10px 8px;
`;

const LoaderCentered = styled.div`
  height: 24px;
  width: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 32px;
  
  svg{
    width: 24px;
    height: 24px;
  }
`;

export {
  UploadWrapper,
  Content,
  Files,
  Actions,
  Title,
  Filename,
  Row,
  CheckIcon,
  ProgressContainer,
  ActionSide,
  RadioAct,
  Error,
  LoaderCentered,
  Warning,
  Form,
  ProggressPing
};
