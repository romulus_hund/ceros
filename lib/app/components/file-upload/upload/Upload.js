import React, {Component} from 'react';
import DropZone from '../dropzone/DropZone';
import {
  Actions,
  ActionSide,
  CheckIcon,
  Content,
  Error,
  Filename,
  Files,
  Form,
  LoaderCentered,
  ProggressPing,
  ProgressContainer,
  RadioAct,
  Row,
  Title,
  UploadWrapper,
  Warning
} from './upload.styled';
import Progress from '../progress/Progress';
import CONSTANTS from '../../../constants';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import ConfirmationDialog from '../../ConfimationDialog';
import validateJSON from '../../../../helpers/validate-json';
import {setExperience} from '../../../../services/api';

const {TEXTS, ENV, ENV_OPTIONS, UPLOAD_MAX_SIZE} = CONSTANTS;

class Upload extends Component {
  constructor(props) {
    super(props);
    this.initialState = {
      files: null,
      uploading: false,
      uploadProgress: {},
      successfullyUploaded: false,
      currEnv: 'staging',
      isRadioActDisabled: false,
      error: null,
      isLoading: false,
      isConfirmationDialogOpen: false,
      aliasName: '',
      estimatedDuration: 0,
      pingProgress: 0
    };
    this.state = {...this.initialState};
    
    this.initValueEnv = {
      staging: ENV.stagingKey,
      production: null
    };
  }
  
  errorHandle = message => {
    if (message) {
      if (message.includes('Warning')) {
        this.setState({
          uploading: false,
          uploadProgress: {},
          successfullyUploaded: false,
          isRadioActDisabled: false,
          error: message
        });
      }
      else {
        this.setState({error: message});
      }
    }
  };
  
  handlePublish = () => {
    if (this.state.files) {
      this.setState({isConfirmationDialogOpen: !this.state.isConfirmationDialogOpen});
    }
  };
  
  clearHandle = () => {
    return () =>
      this.setState({...this.initialState});
  };
  
  handleChangeEnv = e => {
    this.setState({currEnv: e.target.value});
  };
  
  handleChangeAlias = e => {
    this.setState({aliasName: e.target.value});
  };
  
  onFileAdded = files => {
    if (files && files.size > UPLOAD_MAX_SIZE) {
      this.errorHandle('Error: maxFileSize exceeded. Please choose another file');
    }
    else {
      this.setState({files});
    }
  };
  
  handlePublishCancel = () => {
    this.setState({isConfirmationDialogOpen: !this.state.isConfirmationDialogOpen});
  };
  
  handlePublishConfirm = () => {
    const {isConfirmationDialogOpen} = this.state;
    
    // this.props.saveFile(files);
    this.setState({isLoading: true, isConfirmationDialogOpen: !isConfirmationDialogOpen});
    this.uploadFiles().then(() => {
      this.setState({isRadioActDisabled: true});
    }).catch(e => console.log(e));
  };
  
  uploadFiles = async () => {
    this.setState({uploadProgress: {}, uploading: true});
    try {
      await this.sendRequest(this.state.files);
      this.setState({successfullyUploaded: true, uploading: false});
    }
    catch (e) {
      console.log(e);
      this.setState({successfullyUploaded: true, uploading: false});
    }
  };
  
  sendRequest = file => {
    const
      {aliasName, currEnv, uploadProgress} = this.state,
      {version, currentSiteName, sitesList} = this.props,
      
      onUploadProgressObj = {
        onUploadProgress: (event) => {
          if (event.lengthComputable) {
            const copy = {...uploadProgress};
            copy[file.name] = {
              state: 'pending',
              percentage: (event.loaded / event.total) * 100
            };
            this.setState({uploadProgress: copy});
          }
        }
      },
      formData = new FormData();
    
    formData.append('file', file, file.name);
    formData.append('environment', currEnv);
    formData.append('currentSiteName', currentSiteName || '');
    formData.append('sitesList', JSON.stringify(sitesList));
    formData.append('version', version);
    formData.append('rollback', 'false');
    formData.append('jenkins', JSON.stringify({
      'parameter': [
        {'name': 'SITE', 'value': aliasName || undefined},
        {'name': 'ENVIRONMENT', 'value': currEnv},
        {'name': 'VERSION', 'value': version},
        {'name': 'FILE', 'file': 'file0'},
        {'name': 'JSON', 'file': 'file1'},
        {'name': 'ROLLBACK', 'value': 'false'}
      ]
    }));
    if (aliasName.length) {
      formData.append('name', aliasName);
    }
    setExperience(formData, onUploadProgressObj)
      .then(async setExperienceRes => {
        const copy = {...uploadProgress};
        copy[file.name] = {state: 'done', percentage: 100};
        this.setState({
          uploadProgress: copy,
          isLoading: false,
          error: null
        });
        setExperienceRes.data.environment = currEnv;
        this.props.onClose();
        this.props.setNewVersion(setExperienceRes.data);
      })
      .catch(e => {
        const error = validateJSON(e?.request?.response) && JSON.parse(e.request.response);
        if (error && error.error) {
          this.errorHandle(error.error.message);
        }
        this.setState({isLoading: false});
      });
  };
  
  renderPingProgress = () => {
    const {pingProgress} = this.state;
    
    if (pingProgress || pingProgress > 0) {
      return (
        <ProgressContainer>
          <Progress progress={pingProgress}/>
        </ProgressContainer>
      );
    }
  };
  
  renderProgress = file => {
    const {uploading, uploadProgress: uploadProgress1, successfullyUploaded, isLoading} = this.state;
    const uploadProgress = uploadProgress1[file.name];
    if (uploading || successfullyUploaded) {
      return (
        <ProgressContainer>
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0}/>
          {isLoading ?
            <LoaderCentered>
              <CircularProgress style={{width: '24px', height: '24px'}}/>
            </LoaderCentered>
            :
            <CheckIcon
              isvisible={uploadProgress && uploadProgress.state === 'done' ? 'true' : 'false'}
            />
          }
        </ProgressContainer>
      );
    }
  };
  
  renderErrorArray = () => {
    const {files, currEnv, isRadioActDisabled, error, aliasName} = this.state;
    const {name} = files || {};
    
    if (!error.includes('Warning')) {
      return (
        <Files>
          <Row key={name}>
            <Filename>{name}</Filename>
            <Error>{error}</Error>
          </Row>
        </Files>
      );
    }
    else if (files) {
      return (
        <>
          <Files>
            <Row key={name}>
              <Filename>{name}</Filename>
              <Warning>{error}</Warning>
              {this.renderProgress(files)}
            </Row>
          </Files>
          <Form noValidate autoComplete="off">
            <label>{TEXTS.pleaseFillInData}</label>
            <TextField
              id="experience-alias-slug"
              required
              label={TEXTS.experienceAliasSlug}
              variant="outlined"
              size="small"
              fullWidth
              value={aliasName}
              onChange={this.handleChangeAlias}/>
          </Form>
          <RadioAct
            isDisabled={isRadioActDisabled}
            currValue={currEnv}
            initValue={this.initValueEnv}
            handleChange={this.handleChangeEnv}
            options={ENV_OPTIONS}
            title={'Environment'}/>
        </>
      );
    }
  };
  
  renderActionSide = () => {
    const {files, currEnv, isRadioActDisabled, error, estimatedDuration} = this.state;
    const {name} = files || {};
    
    if (error) {
      return this.renderErrorArray();
    }
    else if (files) {
      return (
        <>
          <Files>
            <Row key={name}>
              <Filename>{name}</Filename>
              {this.renderProgress(files)}
            </Row>
          </Files>
          {
            estimatedDuration !== undefined && estimatedDuration > 0 &&
            <ProggressPing>
              <Row key={'please-wait-app-is-being-deployed'}>
                <Filename>{'Please wait: app is being deployed!'}</Filename>
                {this.renderPingProgress()}
              </Row>
            </ProggressPing>
            
          }
          <RadioAct
            isDisabled={isRadioActDisabled}
            currValue={currEnv}
            initValue={this.initValueEnv}
            handleChange={this.handleChangeEnv}
            options={ENV_OPTIONS}
            title={'Environment'}/>
        </>);
    }
  };
  
  isPublishDisabled = () => {
    const {files, uploading, error, successfullyUploaded, aliasName} = this.state;
    
    return !files ||
      uploading ||
      successfullyUploaded ||
      (error && !error.includes('Warning')) ||
      (error && error.includes('Warning') && aliasName.length === 0);
  };
  
  renderActionButtons = () => {
    return (
      <div>
        {this.state.isConfirmationDialogOpen &&
        <ConfirmationDialog
          onConfirm={this.handlePublishConfirm}
          onClose={this.handlePublishCancel}
          open={this.state.isConfirmationDialogOpen}
          content={TEXTS.confirmPublish}
          title={TEXTS.titleConfirmPublish}
        />
        }
        <Button
          size="large"
          color="primary"
          onClick={this.clearHandle()}
          variant="outlined">
          {TEXTS.clear}
        </Button>
        <Button
          data-testid="publish-btn"
          size="large"
          color="primary"
          disabled={this.isPublishDisabled()}
          onClick={this.handlePublish}
          variant="outlined">
          {TEXTS.publish}
        </Button>
      </div>);
  };
  
  render() {
    const {uploading, successfullyUploaded, error} = this.state;
    
    return (
      <UploadWrapper data-testid="upload-panel">
        <Title>{TEXTS.uploadZip}</Title>
        <Content>
          <div>
            <DropZone
              onFileAdded={this.onFileAdded}
              disabled={uploading || successfullyUploaded || error}
            />
          </div>
          <ActionSide>
            {this.renderActionSide()}
          </ActionSide>
        </Content>
        <Actions>
          {this.renderActionButtons()}
        </Actions>
      </UploadWrapper>
    );
  }
}

export default Upload;
