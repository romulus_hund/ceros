import React, {Component} from 'react';
import {DropZoneWrapper, FileInput, Icon} from './drop-zone.styled';
import CONSTANTS from './../../../constants';
const {TEXTS} = CONSTANTS;

class DropZone extends Component {
  constructor(props) {
    super(props);
    this.state = { highlight: false };
    this.fileInputRef = React.createRef();
  }
  
  openFileDialog = () => {
    if (this.props.disabled) return;
    this.fileInputRef.current.click();
  };
  
  onFileAdded = e => {
    const {onFileAdded, disabled} = this.props;
    if (disabled) return;
    onFileAdded(e.target.files[0]);
  };
  
  onDrop = e => {
    e.preventDefault();
    const {onFileAdded, disabled} = this.props;
    if (disabled) return;
    onFileAdded(e.dataTransfer.files[0]);
    this.setState({ highlight: false });
  };
  
  onDragOver = e => {
    e.preventDefault();
    if (this.props.disabled) return;
    this.setState({ highlight: true });
  };
  
  onDragLeave = () => {
    this.setState({ highlight: false });
  };
  
  fileListToArray = list => {
    const array = [];
    for (let i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  };
  
  render() {
    return (
      <DropZoneWrapper
        highlight={this.state.highlight}
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
        onDrop={this.onDrop}
        onClick={this.openFileDialog}
        style={{ cursor: this.props.disabled ? 'default' : 'pointer' }}
      >
        <FileInput
          ref={this.fileInputRef}
          type="file"
          onChange={this.onFileAdded}
        />
        <Icon color="disabled" fontSize="large"/>
        <span>{TEXTS.uploadFiles}</span>
      </DropZoneWrapper>
    );
  }
}

export default DropZone;
