import styled from 'styled-components';

const ProgressWrapper = styled.span`
  width: 100%;
  height: 8px;
  background-color: #90CAF9;
`;

const ProgressBar = styled.img`
  display: block;
  background-color: #0062cc;
  height: 100%;
  margin: 0;
`;

export {
  ProgressWrapper,
  ProgressBar
};
