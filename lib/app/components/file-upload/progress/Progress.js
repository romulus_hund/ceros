import React, { Component } from 'react';
import {ProgressWrapper, ProgressBar} from './progress.styled';

class Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ProgressWrapper>
        <ProgressBar
          style={{ width: this.props.progress + '%' }}
        />
      </ProgressWrapper>
    );
  }
}

export default Progress;
