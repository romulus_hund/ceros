import React from 'react';
import {inject, observer} from 'mobx-react';
import {action} from 'mobx';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import {Centered, ListContainer, LoaderTop, PaddedPaper, StickyPaper, Titles} from './style';
import ExperiencePanel from './experience-panel/ExperiencePanel';
import UploadDataPanel from './upload-data-panel/UploadDataPanel';
import EmptyState from './emptyState/EmptyState';
import CONSTANTS from '../constants';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import {CardArea, CloseX, UploadModal} from './upload-data-panel/upload-data-panel.styled';
import Upload from './file-upload/upload/Upload';

const {TEXTS} = CONSTANTS;

@inject('userStore', 'miniSitesStore')
@observer
export default class Publisher extends React.Component {
  state = {
    open: false
  };
  
  componentDidMount() {
    (async () => {
        await this.props.miniSitesStore.fetchMiniSitesData();
        await this.props.miniSitesStore.initSiteData();
      }
    )();
  }
  
  @action
  sortByProp = () => {
  };
  
  handleModalOpen = () => {
    this.setState({open: true});
  };
  
  handleModalClose = () => {
    this.setState({open: false});
  };
  
  setSiteId = siteId => {
    this.props.miniSitesStore.setCurrentSiteId(siteId);
  };
  
  addNewVersionToStore = data => {
    this.props.miniSitesStore.addNewVersion(data);
  };
  
  handleSetNewVersionResult = data => {
    this.props.miniSitesStore.setNewVersionResult(data);
  };
  
  handleChangeVersionEnv = data => {
    this.props.miniSitesStore.changeVersionEnvironment(data);
  };
  
  handleLastBuildPoling = async data => {
    await this.props.miniSitesStore.lastBuildPoling(data);
  };
  
  createCurrentVersion = () => {
    return `${this.props.miniSitesStore.convertMiniSiteVersions && this.props.miniSitesStore.convertMiniSiteVersions.length + 1}.0.0`;
  };
  
  render() {
    const {
      currentSiteId,
      currentSiteName,
      isLoadingMiniSites,
      convertMiniSiteVersions,
      selectOptions,
      convertedMiniSiteDeployed,
      miniSitesList,
      convertedMiniSitesList
    } = this.props.miniSitesStore;
    
    if (isLoadingMiniSites) {
      return <Centered><CircularProgress/></Centered>;
    }
    
    return (
      <ListContainer>
        <Button
          data-testid={`upload-button`}
          size="small"
          color="primary"
          onClick={this.handleModalOpen}
          variant="outlined"
          disabled={isLoadingMiniSites}
          name='upload-button'
        >
          {TEXTS.upload}
        </Button>
        <UploadModal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={this.state.open}
          onClose={this.handleModalClose}
          closeAfterTransition
          disableEscapeKeyDown
          disableBackdropClick
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.open}>
            <CardArea>
              <CloseX onClick={this.handleModalClose}/>
              <Upload
                currentSiteName={currentSiteName}
                sitesList={convertedMiniSitesList}
                version={this.createCurrentVersion()}
                onClose={this.handleModalClose}
                setNewVersion={this.addNewVersionToStore}
                newVersionResult={this.handleSetNewVersionResult}
              />
            </CardArea>
          </Fade>
        </UploadModal>
        {
          miniSitesList && !miniSitesList.length ?
            <EmptyState title="No micro sites." description="No micro sites found."/>
            :
            <>
              <PaddedPaper>
                <UploadDataPanel
                  currentSiteId={currentSiteId}
                  options={selectOptions}
                  setCurrentSiteId={this.setSiteId}
                  isLoadingMiniSites={isLoadingMiniSites}
                />
              </PaddedPaper>
              <StickyPaper>
                <Titles>
                  <Button size="large" onClick={() => this.sortByProp('version')}>
                    <b>VERSION</b>
                    <KeyboardArrowDown/>
                  </Button>
                  <Button size="large" onClick={() => this.sortByProp('environment')}>
                    <b>Environment</b>
                    <KeyboardArrowDown/>
                  </Button>
                  {/*
              TODO: not for MVP
              <Button size="large" onClick={() => this.sortByProp('schedule')}>
                <b>SCHEDULE</b>
                <KeyboardArrowDown/>
              </Button>*/}
                </Titles>
              </StickyPaper>
              {isLoadingMiniSites ?
                <LoaderTop><CircularProgress/></LoaderTop> :
                convertMiniSiteVersions && convertMiniSiteVersions.length ?
                  convertMiniSiteVersions.map(obj => {
                    const {version} = obj;
                    return <ExperiencePanel
                      key={`${currentSiteId}-${version}`}
                      siteId={currentSiteId}
                      userStore={this.props.userStore}
                      name={currentSiteName}
                      handleChangeVersionEnv={this.handleChangeVersionEnv}
                      handleSetNewVersionResult={this.handleSetNewVersionResult}
                      handleLastBuildPoling={this.handleLastBuildPoling}
                      {...obj}
                      miniSiteDeployed={convertedMiniSiteDeployed}
                    />;
                  }) :
                  <EmptyState title="No versions" description="No versions for this site name found."/>
              }
            </>
        }
      </ListContainer>
    );
  }
}
