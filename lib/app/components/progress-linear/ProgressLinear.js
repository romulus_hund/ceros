import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(() => ({
  root: {
    width: '200px',
    '& > * + *': {
      marginTop: '2px',
    },
  },
}));

export default function ProgressLinear() {
  const classes = useStyles();
  const [completed, setCompleted] = useState(0);
  
  useEffect(() => {
    function progress() {
      setCompleted(oldCompleted => {
        if (oldCompleted === 100) {
          return 0;
        }
        const diff = Math.random() * 10;
        return Math.min(oldCompleted + diff, 100);
      });
    }
    
    const timer = setInterval(progress, 500);
    return () => {
      clearInterval(timer);
    };
  }, []);
  
  return (
    <div>
      <LinearProgress variant="determinate" value={completed} />
     </div>
  );
}
