import styled from 'styled-components';

import Paper from '@material-ui/core/Paper';

const PaddedPaper = styled(Paper)`
  padding: 20px;
  margin-bottom: 20px;
`;

const MainTestDetails = styled(PaddedPaper)`
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  align-items: baseline;
  justify-content: space-around;
  padding: 20px;
  
  div[class*=MuiFormControl] {
    margin: 15px 0;
  }
`;

const Centered = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoaderTop = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: normal;
  margin-top: 100px;
`;

const Titles = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  justify-items: center;
  font-weight: bold;
  font-size: 20px;
  line-height: 48px;
  margin: 0 56px 0 24px;
`;

const StickyPaper = styled(Paper)`
  position: sticky;
  top: 0;
  z-index: 2;
`;

const FilterPaper = styled(Paper)`
  padding: 20px;
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  margin-bottom: 20px;
  
  label {
    margin: 0 15px;
  }
`;

const VariantContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 10px;
  margin-bottom: 20px;
  flex-wrap: wrap;
`;

const ConditionContainer = styled.div`
  margin: 20px 0;
`;

const ListContainer = styled.div`
  margin-bottom: 60px;
  button[name=upload-button] {
    position: absolute;
    top: 17px;
    right: 33%;
  }
`;

const BigTitle = styled.h1`
  margin-top: 20px;
`;

const StyledSection = styled.section`
  margin: 20px 0;
`;

const TestActionButtons = styled.div`
  display: flex;
  place-content: center;
  
  button {
    margin: 20px;
    max-width: 300px;
  }
`;

const ActionPanel = styled.div`
  display: flex;
  align-content: center;
  justify-content: space-between;
  width: 100%;
  padding: 0 7px;

  label {
    height: 2.2em;
    margin-bottom: 0;
  }
  
  legend {
    margin-bottom: 18px;
  }
`;

const LogoWrapper = styled.div`
  margin-bottom: 4px;
  svg{
    margin-right: 10px;
  }
`;

export const HeaderLogo = styled.div`
  height: 35px;
  width: 205px;
  padding: 0 18px 0 0;
  flex: 0 0 auto;
  transition: 0.15s ease-out;
  position: relative;
  svg {
    width: 100%;
    height: 100%;
  }

`;

export {
  PaddedPaper,
  MainTestDetails,
  Titles,
  Centered,
  StickyPaper,
  FilterPaper,
  VariantContainer,
  ConditionContainer,
  StyledSection,
  TestActionButtons,
  ListContainer,
  BigTitle,
  ActionPanel,
  LoaderTop,
  LogoWrapper
};
