import React from 'react';
import DefaultImage from '../../assets/EmptyState.svg';
import {EmptyStateWrapper} from './empty-state.styled';

const EmptyState = ({image, title, description}) => {
	return (
		<EmptyStateWrapper>
			{
				image ? <div style={{backgroundImage: `url(${image})`}}/> : DefaultImage
			}
			<div className="title">{title}</div>
			<div className="description">{description}</div>
		</EmptyStateWrapper>
	);
};

export default EmptyState;
