import * as React from "react";
export function Icon({ width, height, children, ...props }) {
    return (React.createElement("svg", Object.assign({ width: width, height: height }, props),
        React.createElement("g", null, children)));
}
Icon.defaultProps = {
    height: 16,
    width: 16
};
