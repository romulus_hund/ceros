import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import 'jest-styled-components';
import Publisher from './Publisher';
import ExperiencePanel from './experience-panel/ExperiencePanel';
import {Provider} from 'mobx-react';
import { render, fireEvent, wait } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/dom';
import bootstrapper from '../bootstrapper';

const injectables = bootstrapper();

jest.setTimeout(50000);

const providerStores = {
  miniSitesStore: {
    convertedMiniSitesList: [
      {
        name: 'beyondhollywood',
        siteId: 201435,
        title: 'Culture Trip - Beyond Hollywood'
      },
      {
        name: 'ceros-trailsoftheunexpected',
        siteId: 214354,
        title: 'FOOTER PAGES TEST'
      }
    ],
    convertMiniSiteVersions: [
      {
        siteId: 201435,
        name: 'beyondhollywood',
        title: 'Culture Trip - Beyond Hollywood',
        creationDate: 'Mon, 11 Feb 2019 05:18:31 -0500',
        lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
        version: '1.0.0',
        newSite: true
      },
      {
        siteId: 201435,
        name: 'beyondhollywood',
        title: 'Culture Trip - Beyond Hollywood',
        creationDate: 'Mon, 11 Feb 2019 05:18:31 -0500',
        lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
        version: '2.0.0',
        newSite: null
      },
      {
        siteId: 201435,
        name: 'beyondhollywood',
        title: 'Culture Trip - Beyond Hollywood',
        creationDate: 'Mon, 11 Feb 2019 05:18:31 -0500',
        lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
        version: '3.0.0',
        newSite: null
      },
      {
        siteId: 201435,
        name: 'beyondhollywood',
        title: 'Culture Trip - Beyond Hollywood',
        creationDate: 'Mon, 11 Feb 2019 05:18:31 -0500',
        lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
        version: '4.0.0',
        newSite: null
      }
    ],
    convertedMiniSiteDeployed: {
      production: [{version: '2.0.0', status: 'SUCCESS'}],
      staging: [{version: '3.0.0', status: 'SUCCESS'}]
    }
  },
  userStore: null
};

it('check whether publish button is disabled', () => {
  
  const props = {
    version: '2.0.0',
    name: 'beyondhollywood',
    lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
    creationDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
    siteId: 201435,
    miniSiteDeployed: {
      production: [{version: '2.0.0', status: 'SUCCESS'}],
      staging: [{version: '4.0.0', status: 'SUCCESS'}]
    },
    currentSiteName: undefined
  };
  
  const {getByTestId} = render(<ExperiencePanel {...props} />);
  
  fireEvent.click(getByTestId('minisite-version-header-2.0.0'));
  
  const publishButton1 = getByTestId('minisite-version-publish-btn-2.0.0');
  
  expect(publishButton1).not.toBeEnabled();
  
  fireEvent.click(getByTestId('minisite-version-env-staging'));
  
  expect(publishButton1).toBeEnabled();
});

it('check whether publish button is disabled', () => {
  const props = {
    version: '4.0.0',
    name: 'beyondhollywood',
    lastModifiedDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
    creationDate: 'Thu, 12 Dec 2019 10:26:45 -0500',
    siteId: 201435,
    miniSiteDeployed: {
      production: [{version: '2.0.0', status: 'SUCCESS'}],
      staging: [{version: '4.0.0', status: 'SUCCESS'}]
    },
    currentSiteName: undefined
  };
  
  const {getByTestId} = render(<ExperiencePanel {...props} />);
  
  fireEvent.click(getByTestId('minisite-version-header-4.0.0'));
  
  const publishButton = getByTestId('minisite-version-publish-btn-4.0.0');
  
  expect(publishButton).not.toBeEnabled();
  
  fireEvent.click(getByTestId('minisite-version-env-prod'));
  
  expect(publishButton).toBeEnabled();
});

describe('Publisher', () => {
  it('matches the latest snapshot', () => {
    expect(
      toJson(
        shallow(
          <Provider {...providerStores}>
            <Publisher/>
          </Provider>
        )
      )
    ).toMatchSnapshot();
  });
});

describe('check publisher versions',() => {
  
  it('should find list of versions',  async () => {
  
    const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
  
    await injectables.miniSitesStore.fetchMiniSitesData();
    await injectables.miniSitesStore.initSiteData();
    
    await wait(() => expect(getByTestId('minisite-version-1.0.0')).toBeInTheDocument());
    await wait(() => expect(getByTestId('minisite-version-2.0.0')).toBeInTheDocument());
    await wait(() => expect(getByTestId('minisite-version-3.0.0')).toBeInTheDocument());
    await wait(() => expect(getByTestId('minisite-version-4.0.0')).toBeInTheDocument());
    
  });
  
  it('should check versions status',  async () => {
    const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
  
    await injectables.miniSitesStore.fetchMiniSitesData();
    await injectables.miniSitesStore.initSiteData();
    
    const version1 = getByTestId('minisite-version-header-1.0.0');
    const version2 = getByTestId('minisite-version-header-2.0.0');
    const version4 = getByTestId('minisite-version-header-4.0.0');
    
    expect(version1).not.toHaveTextContent('Production');
    expect(version1).not.toHaveTextContent('Staging');
    
    expect(version2).toHaveTextContent('Production');
    expect(version4).toHaveTextContent('Staging');
  });
  
  it('open upload panel',  async () => {
    const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
  
    await injectables.miniSitesStore.fetchMiniSitesData();
    await injectables.miniSitesStore.initSiteData();
   
    const uploadButton = getByTestId('upload-button');
    expect(uploadButton).toBeInTheDocument();
    fireEvent.click(uploadButton);
    expect(getByTestId('upload-panel')).toBeInTheDocument();
    const publishBtn = getByTestId('publish-btn');
    expect(publishBtn).not.toBeEnabled();
  
    const inputEl = document.querySelector('input');
    
    const file = new File(['(⌐□_□)'], 'chucknorris.png', {
      type: 'image/png',
    });
    
    Object.defineProperty(inputEl, 'files', {
      value: [file]
    });
  
    fireEvent.change(inputEl);
    
    // TODO: check file upload functionality
    
  });
  
  it('micro sites list',  async () => {
    const {getByTestId} = render(<Provider><Publisher {...injectables} userStore={null} /></Provider>);
    
    await injectables.miniSitesStore.fetchMiniSitesData();
    await injectables.miniSitesStore.initSiteData();
    
    expect(getByTestId('site-201435')).toBeInTheDocument();
    
    const microSitesSelect = getByTestId('sites-list-select');
    
    expect(getByTestId(microSitesSelect)).toBeInTheDocument();
  
  });
  
});
