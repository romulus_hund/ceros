import React from 'react';
import {mount, shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import 'jest-styled-components';
import Header from '../Header';
import {Provider} from 'mobx-react';
import '@testing-library/jest-dom';
import '@testing-library/dom';
import { render, fireEvent, waitForElement } from '@testing-library/react';

it('renders', () => {
  const {asFragment} = render(<Header />);
  expect(asFragment()).toMatchSnapshot();
});

it('if text exists in a tag', () => {
  const {getByTestId} = render(<Header />);
  
  expect(getByTestId('logoutBtn')).toHaveTextContent('Sign out')
  
});