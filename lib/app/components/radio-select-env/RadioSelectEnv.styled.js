import styled from 'styled-components';
import FormControl from '@material-ui/core/FormControl';

const RadioSelectWrapper = styled(FormControl)`
    margin: 14px 0 0 8px !important;
    
    legend{
     margin: 8px 0;
    }
`;

export {
  RadioSelectWrapper
};
