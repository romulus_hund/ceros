import React from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import PropTypes from 'prop-types';
import {RadioSelectWrapper} from './RadioSelectEnv.styled';
import CONSTANTS from '../../constants';
import Tooltip from '@material-ui/core/Tooltip';

const {ENV, STATUS} = CONSTANTS;

const Wire = ({children, ...props}) => children(props);

function RadioSelectEnv({
  title,
  currValue,
  handleChange,
  isDisabled,
  initValue: {staging, production},
  productionStatus,
  stagingStatus
}) {
  
  return (
    <RadioSelectWrapper component="fieldset" disabled={isDisabled}>
      <FormLabel component="legend">{title}</FormLabel>
      <RadioGroup
        aria-label="environment"
        name="environment"
        value={currValue || staging || production}
        onChange={handleChange}
        row
      >
        <Wire value={ENV.productionKey}>
          {
            props => (
              <Tooltip
                title={!currValue && production === ENV.productionKey ? 'This version is already on production' : ''}
                placement="top-start"
              >
                <FormControlLabel
                  data-testid={`minisite-version-env-prod`}
                  {...props}
                  control={<Radio/>}
                  label={ENV.production}
                  disabled={stagingStatus === STATUS.pending || productionStatus === STATUS.pending}
                />
              </Tooltip>
            )
          }
        </Wire>
        
        <Wire value={ENV.stagingKey}>
          {
            props => (
              <Tooltip title={!currValue && staging === ENV.stagingKey ? 'This version is already on staging' : ''}
                       placement="top-start">
                <FormControlLabel
                  data-testid={`minisite-version-env-staging`}
                  {...props}
                  control={<Radio/>}
                  label={ENV.staging}
                  disabled={stagingStatus === STATUS.pending || productionStatus === STATUS.pending}
                />
              </Tooltip>
            )
          }
        </Wire>
      </RadioGroup>
    </RadioSelectWrapper>
  );
}

RadioSelectEnv.propTypes = {
  title: PropTypes.string,
  currValue: PropTypes.string,
  handleChange: PropTypes.func,
  options: PropTypes.array
};

export default RadioSelectEnv;
