import React, {memo} from 'react';
import {PanelWrapper} from './upload-data-panel.styled';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import CONSTANTS from '../../constants';
import {SiteSelector} from './site-selector.styled';

const UploadDataPanel = ({
  options,
  currentSiteId,
  setCurrentSiteId,
  isLoadingMiniSites
}) => {
  const [selectOpen, setSelectOpen] = React.useState(false),
    
    handleSelectClose = () => {
      setSelectOpen(false);
    },
    handleSelectOpen = () => {
      setSelectOpen(true);
    },
    renderOptions = () => {
      return options && options.map((site, index) => {
        return <MenuItem key={index} value={site.value}><div data-testid={`site-${site.value}`}>{site.label}</div></MenuItem>;
      });
    },
    handleOnChange = e => {
      setCurrentSiteId(e.target.value);
    },
    {TEXTS} = CONSTANTS;
  
  return (
    <PanelWrapper>
      <SiteSelector>
        <InputLabel id="demo-controlled-open-select-label">{TEXTS.microSite}:</InputLabel>
        <Select
          labelid="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={selectOpen}
          onClose={handleSelectClose}
          onOpen={handleSelectOpen}
          value={currentSiteId || ''}
          onChange={handleOnChange}
          disabled={isLoadingMiniSites}
          data-testid="sites-list-select"
        >
          {renderOptions()}
        </Select>
      </SiteSelector>
    </PanelWrapper>
  );
};

export default memo(UploadDataPanel);
