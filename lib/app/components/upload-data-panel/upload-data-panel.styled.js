import styled from 'styled-components';
import Clear from '@material-ui/icons/Clear';
import Modal from '@material-ui/core/Modal';

const PanelWrapper = styled.div`
  display: flex;
  align-content: center;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding: 0 7px;
`;
const CardArea = styled.div`
   display: flex;
   position: relative;
   align-items: center;
   justify-content: center;
   width: 680px;
   height: auto;
   min-width: 400px;
   min-height: 420px;
   background-color: white;
   padding: 10px 30px;
   box-shadow: 0 15px 30px 0 rgba(0, 0, 0, 0.11),
     0 5px 15px 0 rgba(0, 0, 0, 0.08);
   box-sizing: border-box;
`;
const CloseX = styled(Clear)`
  position: absolute;
  top: 6px;
  right: 6px;
  color: #454545;
  cursor: pointer;
`;

const UploadModal = styled(Modal)`
  display: flex!important;
  align-items: center!important;
  justify-content: center!important;
  width: 100%!important;
`;

export {
  PanelWrapper,
  CardArea,
  CloseX,
  UploadModal
};
