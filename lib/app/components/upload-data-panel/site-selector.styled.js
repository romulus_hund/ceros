import styled from 'styled-components';
import FormControl from '@material-ui/core/FormControl';

const SiteSelector = styled(FormControl)`
    flex: 1 0 auto;
    margin-right: 2vw !important;
`;

export {
    SiteSelector
};