import React from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
  static propTypes = {
    placeholder: PropTypes.element
  };

  state = {
    hasError: false
  };

  componentDidCatch(error, info) {
    this.setState({
      hasError: true,
      error
    });
    console.error('CAUGHT ERROR', error, info);
  }

  render() {
    if (this.state.hasError || !this.props.children) {
      return this.state.error.toString() || null;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
