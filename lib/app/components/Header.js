import React from 'react';
import Logo from '../assets/ct-color-emblem-2020.svg';
import {LogoWrapper} from './style';

const Header = () => {
  
  return (
    <nav className="navbar navbar-dark bg-dark">
      <div className="container">
        <a
          href="/"
          className="navbar-brand"
          style={{display: 'flex', alignItems: 'center'}}>
          <LogoWrapper>
            <Logo/>
          </LogoWrapper>
          Ceros Publisher
        </a>
        <div>
          
          <a data-testid='logoutBtn' href="/logout" className="btn btn-dark">
            Sign out
          </a>
        </div>
      </div>
    </nav>
  );
};

export default Header;