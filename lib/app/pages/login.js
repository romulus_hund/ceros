import React from 'react';

export default function Login() {
  return (
    <ul>
      <li>
        <a href="/auth/auth0">Login with Auth0</a>
      </li>
    </ul>
  );
}
