import App, {Container} from 'next/app';

import React from 'react';
import {Provider} from 'mobx-react';
import ErrorBoundry from '../components/ErrorBoundry';
import {MuiThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import getPageContext from '../../helpers/getPageContext';
import bootstrapper from '../bootstrapper';

const injectables = bootstrapper();

export default class MyApp extends App {
  static async getInitialProps({Component, ctx}) {
    let pageProps = {};
    
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    
    const user = ctx.req ? ctx.req.user : null;
    
    return {pageProps, user};
  }
  
  constructor() {
    super();
    this.pageContext = getPageContext();
  }
  
  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }
  
  render() {
    const {Component, pageProps, user} = this.props;
    
    return (
      <Container>
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}>
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}>
            <CssBaseline/>
            <ErrorBoundry placeholder={<div>Something Went Wrong</div>}>
              <Provider userStore={user} {...injectables}>
                <Component
                  pageContext={this.pageContext}
                  user={user}
                  {...pageProps}
                />
              </Provider>
            </ErrorBoundry>
          </MuiThemeProvider>
        </JssProvider>
      </Container>
    );
  }
}
