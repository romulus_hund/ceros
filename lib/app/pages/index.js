import React from 'react';
import PageLayout from '../components/PageLayout';
import Publisher from '../components/Publisher';

export default function Index() {
  return (
    <PageLayout>
      <Publisher />
    </PageLayout>
  );
}
