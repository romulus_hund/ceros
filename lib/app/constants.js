const CONSTANTS = {
  TEXTS: {
    microSite: 'Micro-site',
    upload: 'Upload',
    uploadZip: 'Upload a new Ceros archive (.tar.gz)',
    confirmPublish: `Clicking on this will change the current mini-site version!`,
    titleConfirmPublish: 'Are you sure?',
    clear: 'Clear',
    publish: 'Publish',
    experienceAliasSlug: 'Experience alias slug',
    pleaseFillInData: 'Please fill in the data and send again',
    uploadFiles: 'Select or Drop a file',
  },
  ENV: {
    staging: 'Staging',
    production: 'Production',
    stagingKey: 'staging',
    productionKey: 'production',
  },
  STATUS: {
    published: 'SUCCESS',
    failed: 'FAILURE',
    pending: 'pending'
  },
  ENV_OPTIONS: [
    {value: 'production', label: 'Production'},
    {value: 'staging', label: 'Staging'}
  ],
  UPLOAD_MAX_SIZE: 256000000,
  CT_EMAIL_POSTFIX: '@theculturetrip.com',
  PATTERNS: {
    JSON_ARRAY: '^\\[(".*",?)*]'
  }
};

export default CONSTANTS;
