module.exports = {
  'env': {
    'development': {
      'presets': ['next/babel'],
      'plugins': [
        ['@babel/plugin-proposal-decorators', {'legacy': true}],
        ['@babel/plugin-proposal-class-properties', {'loose': true}],
        ['styled-components', {'ssr': true}],
        ['@babel/plugin-proposal-optional-chaining']
      ]
    },
    'production': {
      'presets': ['next/babel'],
      'plugins': [
        ['@babel/plugin-proposal-decorators', {'legacy': true}],
        ['@babel/plugin-proposal-class-properties', {'loose': true}],
        ['styled-components', {'ssr': true}],
        ['@babel/plugin-proposal-optional-chaining']
      ]
    },
    'test': {
      'presets': [
        '@babel/preset-env',
        '@babel/preset-react',
        ['next/babel', {'preset-env': {'modules': 'commonjs'}}]
      ],
      'plugins': [
        ['@babel/plugin-proposal-decorators', {'legacy': true}],
        ['@babel/plugin-proposal-class-properties', {'loose': true}],
        ['styled-components', {'ssr': true}],
        ['@babel/plugin-proposal-optional-chaining']
      ]
    }
  }
  
};

