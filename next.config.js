const {apiURLs} = require('./lib/config');
const env = process.env.ENVIRONMENT || 'local';
const { BASE_URL: apiUrl } = apiURLs[env];

module.exports = {
    publicRuntimeConfig: {
        API_URL: apiUrl,
        ENV: process.env.DOCKER_ENV
    },
    webpack(config) {
        config.module.rules.push({
            test: /\.svg$/,
            use: ['@svgr/webpack'],
        });

        return config;
    }
};
