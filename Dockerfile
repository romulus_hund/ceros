FROM node:10.15.3-alpine

RUN apk add --update \
    curl \
    php7 \
    php7-simplexml \
    composer \
  && rm -rf /var/cache/apk/*

WORKDIR /app

COPY . /app

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
RUN cd lib/server/php-s3/ && composer install

COPY docker_entrypoint.sh /docker_entrypoint.sh

RUN chmod a+x /docker_entrypoint.sh

EXPOSE 3000

ENTRYPOINT ["/docker_entrypoint.sh"]